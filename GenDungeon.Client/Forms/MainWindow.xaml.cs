﻿using GenDungeon.Domain.ViewModels;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace GenDungeon.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ReactiveWindow<MainWindowViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();

            //ConverterTree.ItemsSource = ViewModel.TreeItems;
            //this.WhenAnyValue(x => x.ConverterTree.SelectedItem)
            //    .BindTo(this, x => x.ViewModel.SelectedItem);

            //using (Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("GenDungeon.Client.JsonSyntax.xshd"))
            //using (var reader = new XmlTextReader(s))
            //{
            //    InputTextBox.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
            //}

            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel, x => x.LogRecordsViewModels, x => x.StructureLogList.ItemsSource)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel, x => x.DungeonStructureImage, x => x.DungeonImage.Source)
                    .DisposeWith(disposable);

                this.Bind(ViewModel, x => x.InputText, x => x.InputTextBox.Text)
                    .DisposeWith(disposable);
                this.Bind(ViewModel, x => x.SelectedLength, x => x.InputTextBox.SelectionLength)
                    .DisposeWith(disposable);
                this.Bind(ViewModel, x => x.SelectedText, x => x.InputTextBox.SelectedText)
                    .DisposeWith(disposable);

                this.Bind(ViewModel, x => x.ImageTabSelected, x => x.ImageTab.IsSelected)
                    .DisposeWith(disposable);
                this.Bind(ViewModel, x => x.LogTabSelected, x => x.LogTab.IsSelected)
                    .DisposeWith(disposable);
                this.Bind(ViewModel, x => x.ConfigTabSelected, x => x.ConfigTab.IsSelected)
                    .DisposeWith(disposable);

                this.OneWayBind(ViewModel, x => x.LogText, x => x.LogTextBox.Text)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel, x => x.OpenConfigFileCommand, x => x.OpenConfigFileButton)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, x => x.SaveBmpFileCommand, x => x.SaveImageButton)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, x => x.GetSourceFolderCommand, x => x.SelectSourceFolderButton)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, x => x.SelectColorSetFileCommand, x => x.SelectColorSetFileButton)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, x => x.GetPictureFolderCommand, x => x.SelectPictureFolder)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, x => x.RefreshProjectCommand, x => x.RefreshButton)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, x => x.GenerateTemplateCommand, x => x.GenerateTemplateMenuItem)
                    .DisposeWith(disposable);
            });
        }
    }
}

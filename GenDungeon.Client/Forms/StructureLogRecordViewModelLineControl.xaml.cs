﻿using GenDungeon.Domain.Lib;
using GenDungeon.Domain.ViewModels;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;

namespace GenDungeon.Forms
{
    /// <summary>
    /// Interaction logic for TemplateTypeLineControl.xaml
    /// </summary>
    public partial class StructureLogRecordViewModelControl : ReactiveUserControl<StructureLogRecordViewModel>
    {
        public StructureLogRecordViewModelControl()
        {
            InitializeComponent();
            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel, x => x.Name, x => x.NameTextBlock.Text)
                    .DisposeWith(disposable);

                NameTextBlock.Events()
                    .MouseLeftButtonDown
                    .ToSignal()
                    .InvokeCommand(ViewModel, x => x.DisplayStructureCommand);
            });
        }
    }
}

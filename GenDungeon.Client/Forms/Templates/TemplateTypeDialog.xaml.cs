﻿using GenDungeon.Domain.ViewModels;
using ReactiveUI;
using System.Reactive.Disposables;

namespace GenDungeon.Forms
{
    /// <summary>
    /// Interaction logic for SortTypeDialog.xaml
    /// </summary>
    public partial class TemplateTypeDialog : ReactiveWindow<TemplateTypeDialogViewModel>
    {
        public TemplateTypeDialog()
        {
            InitializeComponent();
            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel,
                    x => x.TemplateTypes,
                    x => x.TypeList.ItemsSource)
                    .DisposeWith(disposable);
                this.Bind(ViewModel,
                    x => x.SelectedTemplateType,
                    x => x.TypeList.SelectedItem)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel, x => x.AcceptCommand, x => x.AcceptButton)
                    .DisposeWith(disposable);
            });
        }
    }
}

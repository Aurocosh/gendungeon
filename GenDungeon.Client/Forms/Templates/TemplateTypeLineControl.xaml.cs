﻿using GenDungeon.Domain.ViewModels;
using ReactiveUI;
using System.Reactive.Disposables;

namespace GenDungeon.Forms
{
    /// <summary>
    /// Interaction logic for TemplateTypeLineControl.xaml
    /// </summary>
    public partial class TemplateTypeLineControl : ReactiveUserControl<TemplateTypeLineViewModel>
    {
        public TemplateTypeLineControl()
        {
            InitializeComponent();
            this.WhenActivated(disposable =>
            {
                this.OneWayBind(ViewModel, x => x.Name, x => x.DescriptionTextBlock.Text)
                        .DisposeWith(disposable);
            });
        }
    }
}

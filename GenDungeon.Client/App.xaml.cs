﻿using Splat;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ReactiveUI;
using System.Reflection;
using GenDungeon.DialogService;
using GenDungeon.Domain.DialogService;
using GenDungeon.Domain.ViewModels;
using GenDungeon.Forms;

namespace GenDungeon.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IDialogService<ReactiveObject> _dialogService;

        public App()
        {
            Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());

            var dialogService = new DialogService<ReactiveObject>();
            dialogService.RegisterWindowType<MainWindowViewModel, MainWindow>(new WindowViewInitializer<MainWindowViewModel>());
            dialogService.RegisterWindowType<TemplateTypeDialogViewModel, TemplateTypeDialog>(new WindowViewInitializer<TemplateTypeDialogViewModel>());
            WpfInteractions.RegisterInteractions();

            _dialogService = dialogService;
        }

        protected override async void OnStartup(StartupEventArgs eventArgs)
        {
            base.OnStartup(eventArgs);
            var mainWindowViewModel = new MainWindowViewModel(_dialogService);
            await _dialogService.ShowModalPresentationAsync(null, mainWindowViewModel).ConfigureAwait(false);
        }
    }
}

﻿using Microsoft.Win32;
using NumberSorter.Domain.Interactions;
using System.Windows;
using WinColorDialog = System.Windows.Forms.ColorDialog;
using WinDialogResult = System.Windows.Forms.DialogResult;
using FolderDialog = System.Windows.Forms.FolderBrowserDialog;

namespace GenDungeon.Client
{
    static class WpfInteractions
    {
        public static void RegisterInteractions()
        {

            DialogInteractions.PickFolder.RegisterHandler(context =>
            {
                using (var dialog = new FolderDialog())
                {
                    dialog.SelectedPath = context.Input;
                    if (dialog.ShowDialog() == WinDialogResult.OK)
                        context.SetOutput(dialog.SelectedPath);
                    else
                        context.SetOutput("");
                }
            });

            // DialogInteractions.AskYesNoQuestion.RegisterHandler(context =>
            // {
            //     var result = MessageBox.Show(context.Input.Text, context.Input.Header, MessageBoxButton.YesNo, MessageBoxImage.Question);
            //     context.SetOutput(result == MessageBoxResult.Yes);
            // });

            DialogInteractions.FindFileToOpenWithType.RegisterHandler(context =>
            {
                var dialog = new OpenFileDialog { Filter = context.Input };
                if (dialog.ShowDialog() == true)
                    context.SetOutput(dialog.FileName);
                else
                    context.SetOutput("");
            });

            DialogInteractions.FindFileToSaveWithType.RegisterHandler(context =>
            {
                var dialog = new SaveFileDialog { Filter = context.Input };
                if (dialog.ShowDialog() == true)
                    context.SetOutput(dialog.FileName);
                else
                    context.SetOutput("");
            });
        }
    }
}

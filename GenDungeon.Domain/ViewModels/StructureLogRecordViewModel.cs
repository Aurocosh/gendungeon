﻿using GenDungeon.Core.StructureLogger;
using GenDungeon.Domain.StructureLogger;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GenDungeon.Domain.ViewModels
{
    public class StructureLogRecordViewModel
    {
        public int Id { get; }
        public string Name { get; }
        public string Type { get; }
        public Color LogColor { get; }

        public ReactiveCommand<Unit, Unit> DisplayStructureCommand { get; }

        public StructureLogRecordViewModel(StructureLogRecord logRecord, Action<int> displayStructure)
        {
            Id = logRecord.Id;
            Name = logRecord.Name;
            Type = logRecord.StructureType.ToString();
            LogColor = Colors.White;

            DisplayStructureCommand = ReactiveCommand.Create(() => displayStructure(Id));
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using ReactiveUI;
using System.Reactive;
using System.Reactive.Linq;
using ReactiveUI.Fody.Helpers;
using DynamicData;
using GenDungeon.Core.Utility;
using GenDungeon.Domain.Lists;

namespace GenDungeon.Domain.ViewModels
{
    public class TemplateTypeDialogViewModel : ReactiveObject
    {
        #region Fields

        private readonly SourceList<TemplateTypeLineViewModel> _templateTypes = new SourceList<TemplateTypeLineViewModel>();

        #endregion Fields

        #region Properties

        [Reactive] public bool? DialogResult { get; set; }
        [Reactive] public TemplateTypeLineViewModel SelectedTemplateType { get; set; }
        public IEnumerable<TemplateTypeLineViewModel> TemplateTypes => _templateTypes.Items;

        #endregion Properties

        #region Commands

        public ReactiveCommand<Unit, Unit> AcceptCommand { get; }

        #endregion Commands

        #region Constructors

        public TemplateTypeDialogViewModel()
        {
            AcceptCommand = ReactiveCommand.Create(Accept);

            var templateTypes = EnumUtil.GetValues<TemplateType>();
            var templateViewModels = templateTypes
                .Select(x => new TemplateTypeLineViewModel(x, TemplateNamer.GetName(x)))
                .ToList();
            templateViewModels.Sort((x, y) => x.Name.CompareTo(y.Name));
            _templateTypes.AddRange(templateViewModels);

            SelectedTemplateType = TemplateTypes.First(x => x.Type == TemplateType.RandomFill);
        }

        #endregion Constructors

        #region Command functions

        private void Accept()
        {
            DialogResult = SelectedTemplateType != null;
        }
        #endregion Command functions
    }
}

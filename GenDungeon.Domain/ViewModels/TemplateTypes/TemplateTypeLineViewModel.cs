﻿using GenDungeon.Domain.Lists;
using ReactiveUI;

namespace GenDungeon.Domain.ViewModels
{
    public class TemplateTypeLineViewModel : ReactiveObject
    {
        public string Name { get; }
        public TemplateType Type { get; }

        public TemplateTypeLineViewModel(TemplateType type, string name)
        {
            Type = type;
            Name = name;
        }
    }
}

﻿using Combinatorics.Collections;
using DynamicData;
using DynamicData.Binding;
using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Generators;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.RandomProvider;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Core.ShapeProvider;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.Templates;
using GenDungeon.Core.TileSets;
using GenDungeon.Domain.AppColors;
using GenDungeon.Domain.AppColors.Base;
using GenDungeon.Domain.AppColors.Procedural;
using GenDungeon.Domain.AppColors.Static;
using GenDungeon.Domain.DialogService;
using GenDungeon.Domain.Lib;
using GenDungeon.Domain.Lists;
using GenDungeon.Domain.Serialization;
using GenDungeon.Domain.StructureLogger;
using GenDungeon.Domain.Visualization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NumberSorter.Domain.Interactions;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using YamlDotNet.Serialization;

namespace GenDungeon.Domain.ViewModels
{

    public class MainWindowViewModel : ReactiveObject
    {
        #region Fields

        private readonly SourceCache<StructureLogRecord, int> _logRecords;
        public ReadOnlyObservableCollection<StructureLogRecordViewModel> _logRecordsViewModels;

        private readonly IDialogService<ReactiveObject> _dialogService;
        private readonly IDungeonStructureVisualizer _structureVisualizer;
        private readonly Dictionary<int, StructureLogRecord> _logRecordMap;

        #endregion Fields

        #region Properties

        [Reactive] public string LogText { get; set; }
        [Reactive] public string InputText { get; set; }
        [Reactive] public string SourceFolderPath { get; set; }
        [Reactive] public string PictureFolderPath { get; set; }
        [Reactive] public string ColorSetPath { get; set; }
        [Reactive] public string DungeonConfigPath { get; set; }
        //[Reactive] public TreeItemViewModel SelectedItem { get; set; }

        [Reactive] public int SelectedLength { get; set; }
        [Reactive] public string SelectedText { get; set; }
        [Reactive] public WriteableBitmap DungeonStructureImage { get; private set; }
        [Reactive] public ColorSetPallete ColorPallete { get; private set; }

        [Reactive] public bool ConfigTabSelected { get; set; }
        [Reactive] public bool LogTabSelected { get; set; }
        [Reactive] public bool ImageTabSelected { get; set; }

        [Reactive] public bool IsProcessing { get; set; }

        public ReadOnlyObservableCollection<StructureLogRecordViewModel> LogRecordsViewModels => _logRecordsViewModels;

        #endregion Properties

        #region Commands

        public ReactiveCommand<Unit, string> OpenConfigFileCommand { get; }
        public ReactiveCommand<Unit, string> GetSourceFolderCommand { get; }
        public ReactiveCommand<Unit, string> GetPictureFolderCommand { get; }
        public ReactiveCommand<Unit, string> SaveBmpFileCommand { get; }
        public ReactiveCommand<Unit, string> SelectColorSetFileCommand { get; }
        public ReactiveCommand<Unit, string> RefreshProjectCommand { get; }
        public ReactiveCommand<Unit, Unit> GenerateTemplateCommand { get; }
        public ReactiveCommand<Unit, Unit> RegenerateStructuresCommand { get; }

        #endregion Commands

        #region Constructors

        public MainWindowViewModel() : this(null) { }

        public MainWindowViewModel(IDialogService<ReactiveObject> dialogService)
        {
            _logRecords = new SourceCache<StructureLogRecord, int>(x => x.Id);
            _logRecordMap = new Dictionary<int, StructureLogRecord>();

            _dialogService = dialogService;
            _structureVisualizer = new SimpleDungeonVisualizer(1);

            LogText = "";
            InputText = "";
            DungeonConfigPath = "";
            ColorSetPath = Properties.Settings.Default.CurrentColorSetPath;
            PictureFolderPath = Properties.Settings.Default.CurrentPictureFolderPath;
            SourceFolderPath = Properties.Settings.Default.CurrentTileSetFolderPath;

            SelectedLength = 0;
            SelectedText = "";

            ImageTabSelected = true;

            IsProcessing = false;

            DungeonStructureImage = BitmapFactory.New(32, 32);

            var canProcess = this.WhenAnyValue(x => x.IsProcessing)
               .Select(x => !x);

            GetPictureFolderCommand = ReactiveCommand.CreateFromObservable(GetPictureFolder, canProcess);
            GetSourceFolderCommand = ReactiveCommand.CreateFromObservable(GetSourceFolder, canProcess);
            OpenConfigFileCommand = ReactiveCommand.CreateFromObservable(GetConfigFile, canProcess);
            SaveBmpFileCommand = ReactiveCommand.CreateFromObservable(GetSavePathForBmpFile, canProcess);
            SelectColorSetFileCommand = ReactiveCommand.CreateFromObservable(SelectColorSetFile, canProcess);
            RefreshProjectCommand = ReactiveCommand.Create(() => DungeonConfigPath, canProcess);
            GenerateTemplateCommand = ReactiveCommand.Create(GenerateTemplate);
            RegenerateStructuresCommand = ReactiveCommand.Create(RegenerateStructures);

            this.WhenAnyValue(x => x.IsProcessing)
                .Subscribe(x => AddLog("asd:" + x));

            ChangeColorSet(ColorSetPath);

            GetPictureFolderCommand
                .BindTo(this, x => x.PictureFolderPath);
            GetSourceFolderCommand
                .BindTo(this, x => x.SourceFolderPath);
            OpenConfigFileCommand
                .BindTo(this, x => x.DungeonConfigPath);
            SelectColorSetFileCommand
                .BindTo(this, x => x.ColorSetPath);

            SaveBmpFileCommand
                .Subscribe(SaveBmpFile);

            RefreshProjectCommand
                .ObserveOn(RxApp.TaskpoolScheduler)
                .Select(GenerateFromConfig)
                .ObserveOnDispatcher()
                .Subscribe(SetStructure);

            this.WhenAnyValue(x => x.DungeonConfigPath)
                .ToSignal()
                .InvokeCommand(RefreshProjectCommand);
            this.WhenAnyValue(x => x.ColorSetPath)
                .ToSignal()
                .InvokeCommand(RefreshProjectCommand);

            this.WhenAnyValue(x => x.ColorSetPath)
                .Subscribe(ChangeColorSet);
            this.WhenAnyValue(x => x.PictureFolderPath)
                .Subscribe(ChangePictureFolder);
            this.WhenAnyValue(x => x.SourceFolderPath)
                .Subscribe(ChangeSourceFolder);

            var myOperation = _logRecords.Connect()
                .Transform(x => new StructureLogRecordViewModel(x, ShowLogRecord))
                .Sort(SortExpressionComparer<StructureLogRecordViewModel>.Ascending(x => x.Id))
                .ObserveOnDispatcher()
                .Bind(out _logRecordsViewModels)
                .DisposeMany()
                .Subscribe();
        }

        #endregion Constructors

        #region Command functions

        private IObservable<string> GetConfigFile()
        {
            return DialogInteractions.FindFileToOpenWithType.Handle("Yaml files (*.yaml)|*.yaml");
        }

        private IObservable<string> GetSavePathForBmpFile()
        {
            return DialogInteractions.FindFileToSaveWithType.Handle("Bmp (*.bmp)|*.bmp");
        }

        private IObservable<string> SelectColorSetFile()
        {
            return DialogInteractions.FindFileToOpenWithType.Handle("Yaml files (*.yaml)|*.yaml");
        }

        private IObservable<string> GetPictureFolder()
        {
            var path = PictureFolderPath.Length == 0 ? Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) : PictureFolderPath;
            return DialogInteractions.PickFolder.Handle(path);
        }

        private IObservable<string> GetSourceFolder()
        {
            var path = SourceFolderPath.Length == 0 ? Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) : SourceFolderPath;
            return DialogInteractions.PickFolder.Handle(path);
        }

        private DungeonStructure GenerateFromConfig(string filePath)
        {
            _logRecords.Clear();
            _logRecordMap.Clear();

            if (!File.Exists(filePath))
            {
                //SetStructure(new DungeonStructure(), StructureType.DungeonStructure);
                return new DungeonStructure();
            }

            RxApp.MainThreadScheduler.Schedule(() => IsProcessing = true);

            using (var streamReader = new StreamReader(filePath))
            {
                var deserializer = new DeserializerBuilder()
                    .WithTagMapping("!SmoothDungeonGenerator", typeof(SmoothDungeonGenerator))
                    .WithTagMapping("!PerlinDungeonGenerator", typeof(PerlinDungeonGenerator))
                    .WithTagMapping("!CircleDungeonGenerator", typeof(CircleDungeonGenerator))
                    .WithTagMapping("!EvilCaveDungeonGenerator", typeof(CorrodedDungeonGenerator))
                    .WithTagMapping("!ShapesDungeonGenerator", typeof(ShapesDungeonGenerator))
                    .WithTagMapping("!RandomTileDungeonGenerator", typeof(RandomTileDungeonGenerator))
                    .WithTagMapping("!ConstrainedTileDungeonGenerator", typeof(ConstrainedTileDungeonGenerator))
                    .WithTagMapping("!WaveFunctionDungeonGenerator", typeof(WaveFunctionDungeonGenerator))
                    .WithTagMapping("!PatternDungeonGenerator", typeof(PatternDungeonGenerator))

                    .WithTagMapping("!NewRandomProvider", typeof(NewRandomProvider))
                    .WithTagMapping("!FixedRandomProvider", typeof(FixedRandomProvider))
                    .WithTagMapping("!FixedSeedRandomProvider", typeof(FixedSeedRandomProvider))

                    .WithTagMapping("!CircleShapeProvider", typeof(CircleShapeProvider))
                    .WithTagMapping("!SquareShapeProvider", typeof(SquareShapeProvider))
                    .WithTagMapping("!StandardShapeProvider", typeof(StandardShapeProvider))

                    .WithTagMapping("!ColorSetPallete", typeof(ColorSetPallete))

                    .WithTagMapping("!StaticDungeonColorSetProvider", typeof(StaticDungeonColorSetProvider))
                    .WithTagMapping("!PastelColorSetProvider", typeof(PastelColorSetProvider))

                    .WithTagMapping("!RectangleShapeGeneratorFactory", typeof(RectangleShapeGeneratorFactory))
                    .WithTagMapping("!CircleShapeGeneratorFactory", typeof(CircleShapeGeneratorFactory))

                    .Build();
                var value = deserializer.Deserialize<object>(streamReader);
                var converter = (IDungeonGenerator)value;

                var logger = new FileBmpStructureLogger(PictureFolderPath, _structureVisualizer, ColorPallete);
                var tileSetLoader = new TileSetLoader(SourceFolderPath);
                var templateLoader = new TemplateLoader(SourceFolderPath);
                //var logger = new NormalStructureLogger();

                DungeonStructure result = null;
                //try
                //{
                result = converter.Generate(logger, tileSetLoader, templateLoader);
                //}
                //catch (FileNotFoundException e)
                //{
                //    AddLog(e.Message);
                //    AddLog(e.StackTrace);
                //}
                //catch (ArgumentException e)
                //{
                //    AddLog(e.Message);
                //    AddLog(e.StackTrace);
                //}

                //SetStructure(result, StructureType.DungeonStructure);

                RxApp.MainThreadScheduler.Schedule(() => IsProcessing = false);
                return result;

                //var records = logger.GetLogRecords();
                //_logRecords.AddOrUpdate(records);
                //foreach (var record in records)
                //    _logRecordMap[record.Id] = record;
            }

        }

        private void SaveBmpFile(string path)
        {
            CreateThumbnail(path, DungeonStructureImage.Clone());
        }

        void CreateThumbnail(string filename, BitmapSource image5)
        {
            if (filename?.Length != 0)
            {
                using (FileStream stream5 = new FileStream(filename, FileMode.Create))
                {
                    PngBitmapEncoder encoder5 = new PngBitmapEncoder();
                    encoder5.Frames.Add(BitmapFrame.Create(image5));
                    encoder5.Save(stream5);
                }
            }
        }

        private void ShowLogRecord(int recordId)
        {
            //var record = _logRecordMap[recordId];
            //SetStructure(record.Structure, record.StructureType);
        }

        private void SetStructure(DungeonStructure structure)
        {
            var colorSetProvider = ColorPallete.ColorSets[structure.StructureType];
            var colorSet = colorSetProvider.GetColorSet();

            DungeonStructureImage = _structureVisualizer.Init(structure, 0, 0);
            _structureVisualizer.Redraw(DungeonStructureImage, structure, colorSet);
        }

        private void Test()
        {

        }

        private DungeonStructure RefreshProject()
        {
            return GenerateFromConfig(DungeonConfigPath);
        }

        private void RegenerateStructures()
        {
            AddLog("");
        }

        private void ChangeColorSet(string filePath)
        {
            if (File.Exists(filePath))
            {

                using (var streamReader = new StreamReader(filePath))
                {
                    var deserializer = new DeserializerBuilder()
                        .WithTagMapping("!NewRandomProvider", typeof(NewRandomProvider))
                        .WithTagMapping("!FixedRandomProvider", typeof(FixedRandomProvider))
                        .WithTagMapping("!FixedSeedRandomProvider", typeof(FixedSeedRandomProvider))

                        .WithTagMapping("!ColorSetPallete", typeof(ColorSetPallete))

                        .WithTagMapping("!StaticDungeonColorSetProvider", typeof(StaticDungeonColorSetProvider))
                        .WithTagMapping("!PastelColorSetProvider", typeof(PastelColorSetProvider))

                        .Build();
                    ColorPallete = deserializer.Deserialize<ColorSetPallete>(streamReader);
                }
            }
            else
            {
                ColorPallete = new ColorSetPallete();
                ColorPallete.ColorSets[StructureType.DungeonStructure] = new StaticDungeonColorSetProvider();
                ColorPallete.ColorSets[StructureType.RegionMap] = new PastelColorSetProvider();
            }

            Properties.Settings.Default.CurrentColorSetPath = filePath;
            Properties.Settings.Default.Save();
        }


        private void ChangePictureFolder(string filePath)
        {
            Properties.Settings.Default.CurrentPictureFolderPath = filePath;
            Properties.Settings.Default.Save();
        }

        private void ChangeSourceFolder(string filePath)
        {
            Properties.Settings.Default.CurrentTileSetFolderPath = filePath;
            Properties.Settings.Default.Save();
        }
        private void GenerateTemplate()
        {
            var viewModel = new TemplateTypeDialogViewModel();
            _dialogService.ShowModalPresentation(this, viewModel);
            if (viewModel.DialogResult != true || viewModel.SelectedTemplateType == null)
                return;

            var templateType = viewModel.SelectedTemplateType.Type;
            var template = TemplateFactory.GetTemplate(templateType);
            if (template == null)
                return;

            //var temp = new Dictionary<StructureType, string>();
            //temp.Add(StructureType.DungeonStructure, "dungeon");
            //temp.Add(StructureType.RegionMap, "region");

            var serializer = new SerializerBuilder()
                .Build();
            var yaml = serializer.Serialize(template);

            SelectedLength = 0;
            SelectedText = yaml;
            SelectedLength = 0;
            ConfigTabSelected = true;
        }

        #endregion Command functions

        private void AddLog(string logString)
        {
            LogText += "\n" + logString;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Procedural
{
    public sealed class PastelColorSet : RandomMixColorSet
    {
        public PastelColorSet(Random random) : base(random)
        {
        }

        protected override Color GetPositiveBaseColor() => Colors.White;
        protected override Color GetNegativeBaseColor() => Colors.DarkGray;
    }
}

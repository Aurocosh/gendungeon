﻿using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Domain.AppColors.Base;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Procedural
{
    public abstract class RandomMixColorSet : ProceduralColorSet
    {
        private readonly Random _random;

        protected RandomMixColorSet(Random random)
        {
            _random = random;
        }

        protected abstract Color GetPositiveBaseColor();
        protected abstract Color GetNegativeBaseColor();

        protected override Color GenerateNewColor(int colorId)
        {
            var baseColor = colorId > 0 ? GetPositiveBaseColor() : GetNegativeBaseColor();

            int red = _random.Next(256);
            int green = _random.Next(256);
            int blue = _random.Next(256);

            red = (red + baseColor.R) / 2;
            green = (green + baseColor.G) / 2;
            blue = (blue + baseColor.B) / 2;

            return Color.FromRgb((byte)red, (byte)green, (byte)blue);
        }
    }
}

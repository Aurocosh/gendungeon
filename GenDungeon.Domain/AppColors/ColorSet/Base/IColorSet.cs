﻿using System;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Base
{
    public interface IColorSet
    {
        Color GetColor(int colorId);
    }
}

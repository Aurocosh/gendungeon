﻿using System.Collections.Generic;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Base
{
    public abstract class StaticColorSet : IColorSet
    {
        public virtual Color DefaultColor => Colors.White;

        private readonly Dictionary<int, Color> _colorMap;
        public IReadOnlyDictionary<int, Color> ColorMap => _colorMap;

        protected StaticColorSet()
        {
            _colorMap = GetColorMap();
        }

        protected abstract Dictionary<int, Color> GetColorMap();

        public Color GetColor(int colorId)
        {
            if (_colorMap.TryGetValue(colorId, out Color color))
                return color;
            return DefaultColor;
        }
    }
}

﻿using System.Collections.Generic;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Base
{
    public abstract class ProceduralColorSet : IColorSet
    {
        private readonly Dictionary<int, Color> _colorMap;
        public IReadOnlyDictionary<int, Color> ColorMap => _colorMap;

        protected ProceduralColorSet()
        {
            _colorMap = new Dictionary<int, Color>();
        }

        protected abstract Color GenerateNewColor(int colorId);

        public Color GetColor(int colorId)
        {
            if (_colorMap.TryGetValue(colorId, out Color color))
                return color;

            color = GenerateNewColor(colorId);
            _colorMap[colorId] = color;
            return color;
        }
    }
}

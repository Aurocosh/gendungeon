﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Domain.AppColors.Base;
using System.Collections.Generic;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Static
{
    public class StaticDungeonColorSet : StaticColorSet
    {
        protected override Dictionary<int, Color> GetColorMap()
        {
            return new Dictionary<int, Color>
            {
                { (int)TileType.None, Colors.Black },
                { (int)TileType.Floor, Colors.White },
                { (int)TileType.Wall, Colors.DarkGray },
                { (int)TileType.Door, Colors.Lime },
                { (int)TileType.Grass, Colors.Green },
                { (int)TileType.Water, Colors.Blue },
                { (int)TileType.Lava, Colors.OrangeRed },
                { (int)TileType.Road, Colors.LightGray },
            };
        }
    }
}

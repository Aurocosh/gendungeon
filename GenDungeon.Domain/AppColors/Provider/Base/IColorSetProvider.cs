﻿using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Base
{
    public interface IColorSetProvider
    {
        IColorSet GetColorSet();
    }
}

﻿using GenDungeon.Core.Inputs;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Domain.AppColors.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Procedural
{
    public sealed class PastelColorSetProvider : RandomMixColorSetProvier
    {
        protected override IColorSet GenerateColorSet()
        {
            var random = RandomProvider.GetRandom();
            return new PastelColorSet(random);
        }
    }
}

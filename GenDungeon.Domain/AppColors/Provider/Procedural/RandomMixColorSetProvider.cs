﻿using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Domain.AppColors.Base;
using System;
using System.Collections.Generic;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Procedural
{
    public abstract class RandomMixColorSetProvier : IColorSetProvider
    {
        private IColorSet _colorSet;
        public IRandomProvider RandomProvider { get; set; }

        protected abstract IColorSet GenerateColorSet();

        public IColorSet GetColorSet()
        {
            return _colorSet ?? (_colorSet = GenerateColorSet());
        }
    }
}

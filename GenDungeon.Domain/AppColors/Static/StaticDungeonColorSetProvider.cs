﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Domain.AppColors.Base;
using System.Collections.Generic;
using System.Windows.Media;

namespace GenDungeon.Domain.AppColors.Static
{
    public class StaticDungeonColorSetProvider : IColorSetProvider
    {
        public IColorSet GetColorSet()
        {
            return new StaticDungeonColorSet();
        }
    }
}

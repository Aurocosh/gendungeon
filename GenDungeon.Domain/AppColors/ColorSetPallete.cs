﻿using GenDungeon.Core.StructureLogger;
using GenDungeon.Domain.AppColors.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Domain.AppColors
{
    public class ColorSetPallete
    {
        public Dictionary<StructureType, IColorSetProvider> ColorSets { get; set; }

        public ColorSetPallete()
        {
            ColorSets = new Dictionary<StructureType, IColorSetProvider>();
        }
    }
}

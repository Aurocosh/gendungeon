﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Domain.AppColors;
using GenDungeon.Domain.AppColors.Base;
using System.Windows.Media.Imaging;

namespace GenDungeon.Domain.Visualization
{
    public interface IDungeonStructureVisualizer
    {
        WriteableBitmap Init(DungeonStructure structure, int width, int height);
        void Redraw(WriteableBitmap writeableBitmap, DungeonStructure structure, IColorSet colorSet);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using GenDungeon.Core.Dungeon;
using GenDungeon.Domain.AppColors;
using GenDungeon.Domain.AppColors.Base;

namespace GenDungeon.Domain.Visualization
{
    public class SimpleDungeonVisualizer : IDungeonStructureVisualizer
    {
        private int TileSize { get; }

        public SimpleDungeonVisualizer(int tileSize)
        {
            TileSize = tileSize;
        }

        public WriteableBitmap Init(DungeonStructure structure, int width, int height)
        {
            int pixelWidth = structure.Width * TileSize;
            int pixelHeigth = structure.Height * TileSize;
            return BitmapFactory.New(pixelWidth, pixelHeigth);
        }

        public void Redraw(WriteableBitmap writeableBitmap, DungeonStructure structure, IColorSet colorSet)
        {
            using (writeableBitmap.GetBitmapContext())
            {
                int xPos = 0;
                for (int x = 0; x < structure.Width; x++)
                {
                    int yPos = 0;
                    for (int y = 0; y < structure.Height; y++)
                    {
                        var type = structure[x, y];
                        var color = colorSet.GetColor((int)type);
                        writeableBitmap.FillRectangle(xPos, structure.Height - 1 - yPos, xPos + TileSize, structure.Height - 1 - yPos + TileSize, color);
                        yPos += TileSize;
                    }
                    xPos += TileSize;
                }
            }
        }
    }
}

﻿namespace GenDungeon.Domain.DialogService
{
    public interface IViewInitializer
    {
        void Initialize(object view, object viewModel);
    }
}

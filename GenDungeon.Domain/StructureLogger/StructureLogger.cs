﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.StructureLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Domain.StructureLogger
{
    public class NormalStructureLogger : IStructureLogger
    {
        private readonly List<StructureLogRecord> _logRecords;

        private int _nextLogId;

        public StructureLogLevel LogLevel { get; set; }

        public NormalStructureLogger()
        {
            LogLevel = StructureLogLevel.Medium;
            _nextLogId = 0;
            _logRecords = new List<StructureLogRecord>();
        }

        public object GetLogRecords() => new List<StructureLogRecord>(_logRecords);

        public void LogStructure(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            if (LogLevel < LogLevel)
                return;

            int id = _nextLogId++;
            var logRecord = new StructureLogRecord(id, name, logLevel, structureType, structure.DeepCopy());
            _logRecords.Add(logRecord);
        }

        public void LogPartialStructure(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            throw new NotImplementedException();
        }

        public void AddPartialState(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Domain.AppColors;
using GenDungeon.Domain.Visualization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GenDungeon.Domain.StructureLogger
{
    public class MemBmpStructureLogger : IStructureLogger
    {
        private IDungeonStructureVisualizer _structureVisualizer;
        private ColorSetPallete _colorPallete;

        private readonly List<BitmapFrame> _logRecords;

        private int _nextLogId;

        public StructureLogLevel LogLevel { get; set; }

        public MemBmpStructureLogger(IDungeonStructureVisualizer structureVisualizer, ColorSetPallete colorPallete)
        {
            _structureVisualizer = structureVisualizer;
            _colorPallete = colorPallete;
            LogLevel = StructureLogLevel.Medium;
            _nextLogId = 0;
            _logRecords = new List<BitmapFrame>();
        }

        public object GetLogRecords() => new List<BitmapFrame>(_logRecords);

        public void LogStructure(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            if (LogLevel < LogLevel)
                return;

            _logRecords.Add(GenerateBitmap(structure, structureType));
        }

        private BitmapFrame GenerateBitmap(DungeonStructure structure, StructureType structureType)
        {
            var colorSetProvider = _colorPallete.ColorSets[structureType];
            var colorSet = colorSetProvider.GetColorSet();

            var image = _structureVisualizer.Init(structure, 0, 0);
            _structureVisualizer.Redraw(image, structure, colorSet);

            PngBitmapEncoder encoder5 = new PngBitmapEncoder();
            return BitmapFrame.Create(image);
        }

        public void LogPartialStructure(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            throw new NotImplementedException();
        }

        public void AddPartialState(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.StructureLogger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Domain.StructureLogger
{
    public class StructureLogRecord
    {
        public int Id { get; }
        public string Name { get; }
        public StructureType StructureType { get; }
        public StructureLogLevel LogLevel { get; }
        public DungeonStructure Structure { get; }

        public StructureLogRecord(int id, string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            Id = id;
            Name = name;
            LogLevel = logLevel;
            StructureType = structureType;
            Structure = structure;
        }
    }
}

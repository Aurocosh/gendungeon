﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Domain.AppColors;
using GenDungeon.Domain.Visualization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GenDungeon.Domain.StructureLogger
{
    public class FileBmpStructureLogger : IStructureLogger
    {
        private IDungeonStructureVisualizer _structureVisualizer;
        private ColorSetPallete _colorPallete;
        private bool _folderExist;
        private string _folderPath;
        private readonly Dictionary<string, GifBitmapEncoder> _encoders;

        private readonly List<BitmapFrame> _logRecords;

        private int _nextLogId;

        public StructureLogLevel LogLevel { get; set; }

        public FileBmpStructureLogger(string path, IDungeonStructureVisualizer structureVisualizer, ColorSetPallete colorPallete)
        {
            _folderExist = Directory.Exists(path);
            _structureVisualizer = structureVisualizer;
            _colorPallete = colorPallete;
            LogLevel = StructureLogLevel.Medium;
            _nextLogId = 0;
            _logRecords = new List<BitmapFrame>();
            _encoders = new Dictionary<string, GifBitmapEncoder>();

            if (_folderExist)
            {
                var subfolder = DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss");
                _folderPath = Path.Combine(path, subfolder);
                Directory.CreateDirectory(_folderPath);
            }
        }

        public object GetLogRecords() => new List<BitmapFrame>(_logRecords);


        public void AddPartialState(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            if (LogLevel < LogLevel)
                return;

            if (!_encoders.TryGetValue(name, out var encoder))
            {
                encoder = new GifBitmapEncoder();
                _encoders[name] = encoder;
            }

            var frame = GenerateBitmap(structure, structureType);
            encoder.Frames.Add(frame);
        }

        public void LogPartialStructure(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            if (!_folderExist)
                return;
            if (LogLevel < LogLevel)
                return;

            int id = _nextLogId++;
            if (!_encoders.TryGetValue(name, out var encoder))
                encoder = new GifBitmapEncoder();

            var frame = GenerateBitmap(structure, structureType);
            encoder.Frames.Add(frame);

            byte[] gifData;
            using (var ms = new MemoryStream())
            {
                encoder.Save(ms);
                gifData = ms.ToArray();
            }

            byte[] applicationExtention = { 33, 255, 11, 78, 69, 84, 83, 67, 65, 80, 69, 50, 46, 48, 3, 1, 0, 0, 0 };

            var filePath = Path.Combine(_folderPath, $"{id}-{name}.gif");
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                fileStream.Write(gifData, 0, 13);
                fileStream.Write(applicationExtention, 0, applicationExtention.Length);
                fileStream.Write(gifData, 13, gifData.Length - 13);
            }

            _encoders.Remove(name);
        }

        private void SavePicture(int id, string name, string extension, BitmapEncoder encoder)
        {
            var filePath = Path.Combine(_folderPath, $"{id}-{name}.{extension}");
            using (var stream = new FileStream(filePath, FileMode.Create))
                encoder.Save(stream);
        }

        public void LogStructure(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure)
        {
            if (!_folderExist)
                return;
            if (LogLevel < LogLevel)
                return;

            int id = _nextLogId++;
            var frame = GenerateBitmap(structure, structureType);

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(frame);

            var filePath = Path.Combine(_folderPath, $"{id}-{name}.png");
            using (var stream = new FileStream(filePath, FileMode.Create))
                encoder.Save(stream);
        }

        private BitmapFrame GenerateBitmap(DungeonStructure structure, StructureType structureType)
        {
            var colorSetProvider = _colorPallete.ColorSets[structureType];
            var colorSet = colorSetProvider.GetColorSet();

            var image = _structureVisualizer.Init(structure, 0, 0);
            _structureVisualizer.Redraw(image, structure, colorSet);
            return BitmapFrame.Create(image);
        }
    }
}

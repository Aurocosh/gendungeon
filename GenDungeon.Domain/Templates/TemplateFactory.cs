﻿using GenDungeon.Core.Generators;
using ReactiveUI;
using System;
using System.Collections.Generic;

namespace GenDungeon.Domain.Lists
{
    public static class TemplateFactory
    {
        public static object GetTemplate(TemplateType type)
        {
            switch (type)
            {
                case TemplateType.CompleteFill:
                    return new CompleteFillGenerator();
                case TemplateType.RandomFill:
                    return new RandomFillGenerator();

                default:
                    return null;
            }
        }
    }
}
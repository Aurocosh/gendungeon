﻿using System.Collections.Generic;

namespace GenDungeon.Domain.Lists
{
    public static class TemplateNamer
    {
        private static readonly Dictionary<TemplateType, string> _nameMap = new Dictionary<TemplateType, string>();

        static TemplateNamer()
        {
            _nameMap.Add(TemplateType.CompleteFill, "Complete fill");
            _nameMap.Add(TemplateType.RandomFill, "Random fill");
        }

        public static string GetName(TemplateType type) => _nameMap[type];
    }
}
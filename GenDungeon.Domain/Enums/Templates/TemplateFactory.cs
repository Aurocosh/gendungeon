﻿using GenDungeon.Core.Generators;
using GenDungeon.Core.Tools;
using ReactiveUI;
using System;
using System.Collections.Generic;

namespace GenDungeon.Domain.Lists
{
    public static class TemplateFactory
    {
        public static object GetTemplate(TemplateType type)
        {
            switch (type)
            {
                case TemplateType.CompleteFill:
                    return new RectangleFiller();
                case TemplateType.RandomFill:
                    return new SingleValueRandomFiller();

                default:
                    return null;
            }
        }
    }
}
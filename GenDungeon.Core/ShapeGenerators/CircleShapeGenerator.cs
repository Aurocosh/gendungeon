﻿using GenDungeon.Core.Data;
using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ShapeGenerators
{
    public class CircleShapeGenerator : IShapeGenerator
    {
        private readonly Random _random;
        private readonly IntRange _radiusRange;
        public readonly float _extraAreaPercent;

        public bool CanOverlap { get; }

        public CircleShapeGenerator(Random random, IntRange radiusRange, float extraAreaPercent, bool canOverlap)
        {
            _random = random;
            _radiusRange = radiusRange;
            _extraAreaPercent = extraAreaPercent;
            CanOverlap = canOverlap;
        }

        public IEnumerable<Vector2Int> GetShapePoints(Vector2Int origin)
        {
            int shapeRadius = _random.Next(_radiusRange);
            return new SolidCircle(origin, shapeRadius);
        }

        public (IEnumerable<Vector2Int>, IEnumerable<Vector2Int>) GetShapeAndAreaPoints(Vector2Int origin)
        {
            int shapeRadius = _random.Next(_radiusRange);
            int areaRadius = (int)(shapeRadius + (shapeRadius * _extraAreaPercent));
            var shape = new SolidCircle(origin, shapeRadius);
            var area = new SolidCircle(origin, areaRadius);
            return (shape, area);
        }
    }
}

﻿using GenDungeon.Core.Data;
using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ShapeGenerators
{
    public class RectangleShapeGenerator : IShapeGenerator
    {
        private readonly Random _random;
        private readonly IntRange _widthRange;
        private readonly IntRange _heightRange;
        private readonly float _extraAreaPercent;

        public bool CanOverlap { get; }

        public RectangleShapeGenerator(Random random, IntRange widthRange, IntRange heightRange, float extraAreaPercent, bool canOverlap)
        {
            _random = random;
            _widthRange = widthRange;
            _heightRange = heightRange;
            _extraAreaPercent = extraAreaPercent;
            CanOverlap = canOverlap;
        }

        public IEnumerable<Vector2Int> GetShapePoints(Vector2Int origin)
        {
            int width = _random.NextInclusive(_widthRange);
            int heigth = _random.NextInclusive(_heightRange);
            return new SolidRectangle(origin, width, heigth);
        }

        public (IEnumerable<Vector2Int>, IEnumerable<Vector2Int>) GetShapeAndAreaPoints(Vector2Int origin)
        {
            int width = _random.NextInclusive(_widthRange);
            int heigth = _random.NextInclusive(_heightRange);

            int extraAreaWidth = (int)(width * _extraAreaPercent);
            int xShift = extraAreaWidth / 2;
            int areaWidth = width + extraAreaWidth;

            int extraAreaHeigth = (int)(heigth * _extraAreaPercent);
            int yShift = extraAreaHeigth / 2;
            int areaHeigth = heigth + extraAreaHeigth;

            var areaOrigin = origin - new Vector2Int(xShift, yShift);

            var shape = new SolidRectangle(origin, width, heigth);
            var area = new SolidRectangle(areaOrigin, areaWidth, areaHeigth);
            return (shape, area);
        }
    }
}

﻿using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.DataSerialization
{
    public interface IShapeGenerator
    {
        bool CanOverlap { get; }
        IEnumerable<Vector2Int> GetShapePoints(Vector2Int origin);
        (IEnumerable<Vector2Int>, IEnumerable<Vector2Int>) GetShapeAndAreaPoints(Vector2Int origin);
    }
}

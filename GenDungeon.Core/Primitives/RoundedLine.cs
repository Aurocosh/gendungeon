﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class RoundedLine : IEnumerable<Vector2Int>
    {
        private ThinLine Line { get; }
        private int Radius { get; }

        public RoundedLine(ThinLine line, int radius)
        {
            Line = line;
            Radius = radius;
        }

        public RoundedLine(Vector2Int start, Vector2Int stop, int radius) : this(new ThinLine(start, stop), radius)
        {
        }

        public IEnumerable<Vector2Int> Iterator => Iterate(Line, Radius);

        private static IEnumerable<Vector2Int> Iterate(ThinLine line, int radius)
        {
            return line.Select(x => new SolidCircle(x, radius).Iterator).Aggregate(Enumerable.Empty<Vector2Int>(), (a, b) => a.Concat(b));
        }

        public IEnumerator<Vector2Int> GetEnumerator() => Iterator.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}

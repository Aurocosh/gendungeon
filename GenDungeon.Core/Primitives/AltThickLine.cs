﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class AltThickLine : IEnumerable<Vector2Int>
    {
        private ThinLine Line { get; }
        private int Radius { get; }

        public AltThickLine(ThinLine line, int radius)
        {
            Line = line;
            Radius = radius;
        }

        public AltThickLine(Vector2Int start, Vector2Int stop, int radius) : this(new ThinLine(start, stop), radius)
        {
        }

        public IEnumerable<Vector2Int> Iterator => Iterate(Line, Radius);

        private static IEnumerable<Vector2Int> Iterate(ThinLine line, int radius)
        {
            var startX = line.Start.X;
            var startY = line.Start.Y;

            var endX = line.Stop.X;
            var endY = line.Stop.Y;

            int dx = Math.Abs(endX - startX);
            int dy = Math.Abs(endY - startY);

            int sx = startX < endX ? 1 : -1;
            int sy = startY < endY ? 1 : -1;

            int y2;
            int err = dx - dy;
            float ed = dx + dy == 0 ? 1 : (float)Math.Sqrt((dx * dx) + (dy * dy));

            for (radius = (radius + 1) / 2; ;)
            {
                yield return new Vector2Int(startX, startY);
                int e2 = err;
                int x2 = startX;
                if (2 * e2 >= -dx)
                {
                    for (e2 += dy, y2 = startY; e2 < ed * radius && (endY != y2 || dx > dy); e2 += dx)
                        yield return new Vector2Int(startX, y2 += sy);

                    if (startX == endX)
                        break;
                    e2 = err;
                    err -= dy;
                    startX += sx;
                }
                if (2 * e2 <= dy)
                {                                            /* y step */
                    for (e2 = dx - e2; e2 < ed * radius && (endX != x2 || dx < dy); e2 += dy)
                        yield return new Vector2Int(x2 += sx, startY);
                    if (startY == endY)
                        break;
                    err += dx;
                    startY += sy;
                }
            }

        }

        public IEnumerator<Vector2Int> GetEnumerator() => Iterator.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
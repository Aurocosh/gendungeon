﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class ThinLine : IEnumerable<Vector2Int>
    {
        public Vector2Int Start { get; }
        public Vector2Int Stop { get; }

        public ThinLine(Vector2Int start, Vector2Int stop)
        {
            Start = start;
            Stop = stop;
        }

        public ThinLine(int startX, int startY, int stopX, int stopY) : this(new Vector2Int(startX, startY), new Vector2Int(stopX, stopY))
        {
        }

        public IEnumerable<Vector2Int> Iterator => Iterate(Start, Stop);

        private static IEnumerable<Vector2Int> Iterate(Vector2Int from, Vector2Int to)
        {
            int x = from.X;
            int y = from.Y;

            int dx = to.X - from.X;
            int dy = to.Y - from.Y;

            var inverted = false;
            int step = Math.Sign(dx);
            int gradientStep = Math.Sign(dy);

            int longest = Math.Abs(dx);
            int shortest = Math.Abs(dy);

            if (longest < shortest)
            {
                inverted = true;
                longest = Math.Abs(dy);
                shortest = Math.Abs(dx);

                step = Math.Sign(dy);
                gradientStep = Math.Sign(dx);
            }

            int gradientAccumulation = longest / 2;
            for (var i = 0; i < longest; i++)
            {
                yield return new Vector2Int(x, y);

                if (inverted)
                    y += step;
                else
                    x += step;

                gradientAccumulation += shortest;
                if (gradientAccumulation >= longest)
                {
                    if (inverted)
                        x += gradientStep;
                    else
                        y += gradientStep;
                    gradientAccumulation -= longest;
                }
            }
        }

        public IEnumerator<Vector2Int> GetEnumerator() => Iterator.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}

﻿using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public interface ICoordinateShape : IEnumerable<Vector2Int>
    {
        bool IsInBounds(int x, int y);
        bool IsInBounds(Vector2Int point);
    }
}

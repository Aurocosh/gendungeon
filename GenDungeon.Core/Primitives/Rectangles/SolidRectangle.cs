﻿using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class SolidRectangle : ICoordinateShape
    {
        public int X { get; }
        public int Y { get; }
        public int Width { get; }
        public int Height { get; }

        public int LimitX => X + Width;
        public int LimitY => Y + Height;

        public Rectangle Rectangle => new Rectangle(X, Y, Width, Height);

        public SolidRectangle(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public SolidRectangle(Vector2Int origin, int width, int height) : this(origin.X, origin.Y, width, height)
        {
        }

        public SolidRectangle(Rectangle rectangle) : this(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height)
        {
        }

        public IEnumerable<Vector2Int> Iterator => IterateRows();

        public IEnumerable<Vector2Int> IterateColumns()
        {
            var maxX = LimitX;
            var maxY = LimitY;

            for (int x = X; x < maxX; x++)
            {
                for (int y = Y; y < maxY; y++)
                {
                    yield return new Vector2Int(x, y);
                }
            }
        }

        public IEnumerable<Vector2Int> IterateRows()
        {
            var maxX = LimitX;
            var maxY = LimitY;

            for (int y = Y; y < maxY; y++)
            {
                for (int x = X; x < maxX; x++)
                {
                    yield return new Vector2Int(x, y);
                }
            }
        }

        public IEnumerator<Vector2Int> GetEnumerator() => Iterator.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public bool IsInBounds(int x, int y)
        {
            return x >= X && y >= Y && x < LimitX && y < LimitY;
        }

        public bool IsInBounds(Vector2Int point)
        {
            return point.X >= X && point.Y >= Y && point.X < LimitX && point.Y < LimitY;
        }
    }
}

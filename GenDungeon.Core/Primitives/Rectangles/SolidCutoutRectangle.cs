﻿using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class SolidCutoutRectangle : ICoordinateShape
    {
        public int X { get; }
        public int Y { get; }
        public int Width { get; }
        public int Height { get; }

        public int CutX { get; }
        public int CutY { get; }
        public int CutWidth { get; }
        public int CutHeight { get; }

        public int MaxX => X + Width;
        public int MaxY => Y + Height;

        public int CutMaxX => CutX + CutWidth;
        public int CutMaxY => CutY + CutHeight;

        public Rectangle Rectangle => new Rectangle(X, Y, Width, Height);
        public Rectangle CutoutRectangle => new Rectangle(CutX, CutY, CutWidth, CutHeight);

        public SolidCutoutRectangle(int x, int y, int width, int height, int cutX, int cutY, int cutWidth, int cutHeight)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;

            CutX = cutX;
            CutY = cutY;
            CutWidth = cutWidth;
            CutHeight = cutHeight;

            ValidateInput();
        }

        public SolidCutoutRectangle(Rectangle rectangle, Rectangle cutout)
        {
            X = rectangle.X;
            Y = rectangle.Y;
            Width = rectangle.Width;
            Height = rectangle.Height;

            CutX = cutout.X;
            CutY = cutout.Y;
            CutWidth = cutout.Width;
            CutHeight = cutout.Height;

            ValidateInput();
        }

        private void ValidateInput()
        {
            if (CutX > CutMaxX)
                throw new ArgumentException("CutMinX cannot be bigger then CutMaxX");
            if (CutY > CutMaxY)
                throw new ArgumentException("CutMinY cannot be bigger then CutMaxY");

            if (CutX < X || CutMaxX > MaxX || CutY < Y || CutMaxY > MaxY)
                throw new ArgumentException("Cutout does not fit into the rectangle");
        }

        public IEnumerable<Vector2Int> Iterator => IterateRows();

        public IEnumerable<Vector2Int> IterateColumns()
        {
            var maxX = MaxX;
            var maxY = MaxY;

            for (int x = X; x < maxX; x++)
            {
                for (int y = Y; y < maxY; y++)
                {
                    if (x > CutX && x < CutMaxX && y > CutY && y < CutMaxY)
                        x += CutMaxX - CutX;
                    yield return new Vector2Int(x, y);
                }
            }
        }

        public IEnumerable<Vector2Int> IterateRows()
        {
            var maxX = MaxX;
            var maxY = MaxY;

            for (int y = Y; y < maxY; y++)
            {
                for (int x = X; x < maxX; x++)
                {
                    if (x > CutX && x < CutMaxX && y > CutY && y < CutMaxY)
                        y += CutMaxY - CutY;
                    yield return new Vector2Int(x, y);
                }
            }
        }

        public IEnumerator<Vector2Int> GetEnumerator() => Iterator.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public bool IsInBounds(int x, int y)
        {
            return x >= 0 && y >= 0 && x < Width && y < Height;
        }

        public bool IsInBounds(Vector2Int point)
        {
            return point.X >= 0 && point.Y >= 0 && point.X < Width && point.Y < Height;
        }
    }
}

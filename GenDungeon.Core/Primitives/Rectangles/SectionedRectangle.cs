﻿using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class SectionedRectangle : SolidRectangle
    {
        public int SectionWidth { get; }
        public int SectionHeight { get; }

        public SectionedRectangle(int x, int y, int width, int height, int sectionWidth, int sectionHeight) : base(x, y, width, height)
        {
            SectionWidth = sectionWidth;
            SectionHeight = sectionHeight;
        }

        public SectionedRectangle(Vector2Int origin, int width, int height, int sectionWidth, int sectionHeight) : this(origin.X, origin.Y, width, height, sectionWidth, sectionHeight)
        {
        }

        public SectionedRectangle(Rectangle rectangle, int sectionWidth, int sectionHeight) : this(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height, sectionWidth, sectionHeight)
        {
        }

        public IEnumerable<SolidRectangle> IterateSections()
        {
            int sectionCountX = (int)Math.Ceiling(Width / (float)SectionWidth);
            int sectionCountY = (int)Math.Ceiling(Height / (float)SectionHeight);

            for (int x = 0; x < sectionCountX; x++)
            {
                int startX = SectionWidth * x;
                int width = Math.Min(Width - startX, SectionWidth);
                for (int y = 0; y < sectionCountY; y++)
                {
                    int startY = SectionWidth * y;
                    int height = Math.Min(Height - startY, SectionHeight);
                    yield return new SolidRectangle(startX, startY, width, height);
                }
            }
        }
    }
}

﻿using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class PolyLinePoints : IEnumerable<Vector2Int>
    {
        private readonly List<Vector2Int> _points;

        public bool ConnectedEnds { get; }
        public IEnumerable<Vector2Int> Points => _points;
        public IEnumerable<Vector2Int> Iterator => Iterate(GetLines());

        public PolyLinePoints(IEnumerable<Vector2Int> points, bool connectedEnds = false)
        {
            _points = points.ToList();
            ConnectedEnds = connectedEnds;
        }

        public List<ThinLine> GetLines()
        {
            int pointCount = _points.Count;
            int lineCount = (int)Math.Ceiling(pointCount / 2f);
            var lines = new List<ThinLine>(lineCount);
            if (pointCount < 2)
                return lines;

            var currentStart = _points[0];
            for (int i = 1; i < pointCount; i++)
            {
                var currentEnd = _points[i];
                lines.Add(new ThinLine(currentStart, currentEnd));
                currentStart = currentEnd;
            }

            if (ConnectedEnds)
                lines.Add(new ThinLine(currentStart, _points[0]));

            return lines;
        }

        private static IEnumerable<Vector2Int> Iterate(List<ThinLine> lines)
        {
            if (lines.Count == 0)
                return Enumerable.Empty<Vector2Int>();
            return lines.Select(x => x.Iterator).Aggregate(Enumerable.Empty<Vector2Int>(), (a, b) => a.Concat(b));
        }

        public IEnumerator<Vector2Int> GetEnumerator() => Iterator.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}

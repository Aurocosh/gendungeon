﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class ThickLine : IEnumerable<Vector2Int>
    {
        private ThinLine Line { get; }
        private int Radius { get; }

        public ThickLine(ThinLine line, int radius)
        {
            Line = line;
            Radius = radius;
        }

        public ThickLine(Vector2Int start, Vector2Int stop, int radius) : this(new ThinLine(start, stop), radius)
        {
        }

        public IEnumerable<Vector2Int> Iterator => Iterate(Line, Radius);

        private static IEnumerable<Vector2Int> Iterate(ThinLine line, int radius)
        {
            return DrawThickLine(line.Start.X, line.Start.Y, line.Stop.X, line.Stop.Y, radius, LineThicknessMode.LineThicknessMiddle);
        }

        public IEnumerator<Vector2Int> GetEnumerator() => Iterator.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private static IEnumerable<Vector2Int> DrawLineOverlap(int startX, int startY, int endX, int endY, LineOverlapMode overlap)
        {
            int stepX, stepY;
            //calculate direction
            int deltaX = endX - startX;
            int deltaY = endY - startY;
            if (deltaX < 0)
            {
                deltaX = -deltaX;
                stepX = -1;
            }
            else
            {
                stepX = +1;
            }
            if (deltaY < 0)
            {
                deltaY = -deltaY;
                stepY = -1;
            }
            else
            {
                stepY = +1;
            }
            int deltaXTimes2 = deltaX << 1;
            int deltaYTimes2 = deltaY << 1;
            //draw start pixel
            yield return new Vector2Int(startX, startY);
            if (deltaX > deltaY)
            {
                // start value represents a half step in Y direction
                int error = deltaYTimes2 - deltaX;
                while (startX != endX)
                {
                    // step in main direction
                    startX += stepX;
                    if (error >= 0)
                    {
                        if (overlap == LineOverlapMode.LineOverlapMajor)
                        {
                            // draw pixel in main direction before changing
                            yield return new Vector2Int(startX, startY);
                        }
                        // change Y
                        startY += stepY;
                        if (overlap == LineOverlapMode.LineOverlapMinor)
                        {
                            // draw pixel in minor direction before changing
                            yield return new Vector2Int(startX - stepX, startY);
                        }
                        error -= deltaXTimes2;
                    }
                    error += deltaYTimes2;
                    yield return new Vector2Int(startX, startY);
                }
            }
            else
            {
                int error = deltaXTimes2 - deltaY;
                while (startY != endY)
                {
                    startY += stepY;
                    if (error >= 0)
                    {
                        if (overlap == LineOverlapMode.LineOverlapMajor)
                        {
                            // draw pixel in main direction before changing
                            yield return new Vector2Int(startX, startY);
                        }
                        startX += stepX;
                        if (overlap == LineOverlapMode.LineOverlapMinor)
                        {
                            // draw pixel in minor direction before changing
                            yield return new Vector2Int(startX, startY - stepY);
                        }
                        error -= deltaYTimes2;
                    }
                    error += deltaXTimes2;
                    yield return new Vector2Int(startX, startY);
                }
            }
        }

        /**
		 * Bresenham with thickness
		 * no pixel missed and every pixel only drawn once!
		 */
        private static IEnumerable<Vector2Int> DrawThickLine(int startX, int startY, int endX, int endY, int thickness, LineThicknessMode thicknessMode)
        {
            if (thickness <= 1)
                return DrawLineOverlap(startX, startY, endX, endY, LineOverlapMode.LineOverlapNone);

            int stepX, stepY;
            int deltaY = endX - startX;
            int deltaX = endY - startY;

            bool swap = true;
            if (deltaX < 0)
            {
                deltaX = -deltaX;
                stepX = -1;
                swap = !swap;
            }
            else
            {
                stepX = +1;
            }
            if (deltaY < 0)
            {
                deltaY = -deltaY;
                stepY = -1;
                swap = !swap;
            }
            else
            {
                stepY = +1;
            }

            int deltaXTimes2 = deltaX << 1;
            int deltaYTimes2 = deltaY << 1;

            int drawStartAdjustCount = thickness / 2;
            if (thicknessMode == LineThicknessMode.LineThicknessDrawCounterclockwise)
                drawStartAdjustCount = thickness - 1;
            else if (thicknessMode == LineThicknessMode.LineThicknessDrawCclockwise)
                drawStartAdjustCount = 0;

            if (deltaX >= deltaY)
            {
                if (swap)
                {
                    drawStartAdjustCount = thickness - 1 - drawStartAdjustCount;
                    stepY = -stepY;
                }
                else
                {
                    stepX = -stepX;
                }

                // No pixel will be missed if LineOverlapMajor is used
                int error = deltaYTimes2 - deltaX;
                for (int i = drawStartAdjustCount; i > 0; i--)
                {
                    startX -= stepX;
                    endX -= stepX;
                    if (error >= 0)
                    {
                        startY -= stepY;
                        endY -= stepY;
                        error -= deltaXTimes2;
                    }
                    error += deltaYTimes2;
                }

                var lineIterator = new ThinLine(startX, startY, endX, endY).Iterator;
                error = deltaYTimes2 - deltaX;
                for (int i = thickness; i > 1; i--)
                {
                    startX += stepX;
                    endX += stepX;
                    LineOverlapMode overlap = LineOverlapMode.LineOverlapNone;
                    if (error >= 0)
                    {
                        startY += stepY;
                        endY += stepY;
                        error -= deltaXTimes2;
                        overlap = LineOverlapMode.LineOverlapMajor;
                    }
                    error += deltaYTimes2;
                    lineIterator = lineIterator.Concat(DrawLineOverlap(startX, startY, endX, endY, overlap));
                }
                return lineIterator;
            }
            else
            {
                if (swap)
                {
                    stepX = -stepX;
                }
                else
                {
                    drawStartAdjustCount = thickness - 1 - drawStartAdjustCount;
                    stepY = -stepY;
                }

                int error = deltaXTimes2 - deltaY;
                for (int i = drawStartAdjustCount; i > 0; i--)
                {
                    startY -= stepY;
                    endY -= stepY;
                    if (error >= 0)
                    {
                        startX -= stepX;
                        endX -= stepX;
                        error -= deltaYTimes2;
                    }
                    error += deltaXTimes2;
                }

                var lineIterator = new ThinLine(startX, startY, endX, endY).Iterator;
                error = deltaXTimes2 - deltaY;
                for (int i = thickness; i > 1; i--)
                {
                    startY += stepY;
                    endY += stepY;
                    LineOverlapMode overlap = LineOverlapMode.LineOverlapNone;
                    if (error >= 0)
                    {
                        startX += stepX;
                        endX += stepX;
                        error -= deltaYTimes2;
                        overlap = LineOverlapMode.LineOverlapMajor;
                    }
                    error += deltaXTimes2;
                    lineIterator = lineIterator.Concat(DrawLineOverlap(startX, startY, endX, endY, overlap));
                }
                return lineIterator;
            }
        }
    }
}

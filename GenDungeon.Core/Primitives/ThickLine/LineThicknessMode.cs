﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public enum LineThicknessMode
    {
        LineThicknessMiddle = 0,                    // Start point is on the line at center of the thick line
        LineThicknessDrawCclockwise = 1,            // Start point is on the counter clockwise border line
        LineThicknessDrawCounterclockwise = 2       // Start point is on the clockwise border line
    }
}

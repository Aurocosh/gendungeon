﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    /*
    * Overlap means drawing additional pixel when changing minor direction
    * Needed for drawThickLine, otherwise some pixels will be missing in the thick line
    */
    public enum LineOverlapMode
    {
        LineOverlapNone = 0,    // No line overlap, like in standard Bresenham
        LineOverlapMajor = 1,   // Overlap - first go major then minor direction. Pixel is drawn as extension after actual line
        LineOverlapMinor = 2,   // Overlap - first go minor then major direction. Pixel is drawn as extension before next line
        LineOverlapBoth = 3     // Overlap - both
    }
}

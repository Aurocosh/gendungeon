﻿using GenDungeon.Core.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Primitives
{
    public class SolidCircle : ICoordinateShape
    {
        private readonly int _radiusSquared;

        public int Radius { get; }
        public Vector2Int Center { get; }

        public SolidCircle(Vector2Int origin, int radius)
        {
            _radiusSquared = radius * radius;

            Center = origin;
            Radius = radius;
        }

        public SolidCircle(int x, int y, int radius) : this(new Vector2Int(x, y), radius)
        {
        }

        public IEnumerable<Vector2Int> Iterator => Iterate(Center);

        private IEnumerable<Vector2Int> Iterate(Vector2Int origin)
        {
            for (int x = -Radius; x <= Radius; x++)
            {
                for (int y = -Radius; y <= Radius; y++)
                {
                    if (IsInBounds(x, y))
                        yield return origin + new Vector2Int(x, y);
                }
            }
        }

        public IEnumerator<Vector2Int> GetEnumerator() => Iterator.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public bool IsInBounds(int x, int y)
        {
            return (x * x) + (y * y) <= _radiusSquared;
        }

        public bool IsInBounds(Vector2Int point)
        {
            return point.LengthSquared() <= _radiusSquared;
        }
    }
}

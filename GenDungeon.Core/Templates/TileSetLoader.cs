﻿using GenDungeon.Core.Data.Tiles;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.InputData.BitmapDungeonMapper;
using GenDungeon.Core.InputData.Tiles;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace GenDungeon.Core.Templates
{
    public class TemplateLoader : ITemplateLoader
    {
        private readonly string _sourceFolder;
        private readonly IDeserializer _deserializer;

        public TemplateLoader(string sourceFolder)
        {
            _sourceFolder = sourceFolder;

            _deserializer = new DeserializerBuilder()
                .WithTagMapping("!ColorMapping", typeof(ColorMapping))
                .Build();
        }

        public DungeonStructure LoadTemplate(string name)
        {
            var templateFolder = Path.Combine(_sourceFolder, "Templates");

            var templateFiles = Directory.GetFiles(templateFolder, $"{name}.*", SearchOption.AllDirectories);
            if (templateFiles.Length == 0)
                return new DungeonStructure();

            var templateFilePath = templateFiles[0];
            var tileDirectory = Path.GetDirectoryName(templateFilePath);

            var colorSetFile = Path.Combine(tileDirectory, "ColorMapper.yaml");
            var colorMapping = LoadYaml<ColorMapping>(colorSetFile);
            var bitmapMapper = new ColorBitmapDungeonMapper(colorMapping.ToIntMapping());

            using (var bitmap = new Bitmap(templateFilePath))
                return bitmapMapper.Convert(bitmap);
        }

        private T LoadYaml<T>(string filePath)
        {
            using (var streamReader = new StreamReader(filePath))
                return _deserializer.Deserialize<T>(streamReader);
        }
    }
}

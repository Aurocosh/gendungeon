﻿using GenDungeon.Core.Dungeon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Templates
{
    public interface ITemplateLoader
    {
        DungeonStructure LoadTemplate(string name);
    }
}

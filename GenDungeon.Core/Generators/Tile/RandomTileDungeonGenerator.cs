﻿using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.PerlinNoize;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.Templates;
using GenDungeon.Core.TileSets;
using GenDungeon.Core.TileSets.Painter;
using GenDungeon.Core.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Generators
{
    public class RandomTileDungeonGenerator : IDungeonGenerator
    {
        public int Width { get; set; }
        public int Heigth { get; set; }

        public string TileSet { get; set; }

        public IRandomProvider RandomProvider { get; set; }

        public DungeonStructure Generate(IStructureLogger structureLogger, ITileSetLoader tileSetLoader, ITemplateLoader templateLoader)
        {
            var schema = new DungeonStructure(Width, Heigth);

            var tileSet = tileSetLoader.LoadTileSet(TileSet);

            var randomShapeFiller = new MultiValueRandomShapeFiller();
            randomShapeFiller.Fill(schema, tileSet.TileIds);
            structureLogger.LogStructure("Tile schema", StructureLogLevel.Medium, StructureType.RegionMap, schema);

            var tileSetPainter = new TileSetPainter(tileSet);
            var structure = tileSetPainter.PaintDungeon(schema);
            structureLogger.LogStructure("Tile set painted", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            return structure;
        }
    }
}

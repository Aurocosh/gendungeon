﻿using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.DataSerialization.ShapeGeneratorFactory;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.PerlinNoize;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.Templates;
using GenDungeon.Core.TileSets;
using GenDungeon.Core.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Generators
{
    public class ShapesDungeonGenerator : IDungeonGenerator
    {
        public int Width { get; set; }
        public int Heigth { get; set; }

        public TileType FloorType { get; set; }
        public TileType WallType { get; set; }

        public int BorderSize { get; set; }
        public int AttemptLimit { get; set; }

        public int MinimumRoomSize { get; set; }
        public int MinimumWallSize { get; set; }

        public bool SmoothBufferInput { get; set; }
        public int SmoothIterationCount { get; set; }

        public bool CorrosionBufferInput { get; set; }
        public float CorrosionProbability { get; set; }
        public int CorrosionIterationCount { get; set; }

        public int ShapeCount { get; set; }

        public IRandomProvider RandomProvider { get; set; }
        public List<LineConnectionData> ConnectionTypes { get; set; }
        public List<IShapeGeneratorFactory> ShapeGenerators { get; set; }

        public DungeonStructure Generate(IStructureLogger structureLogger, ITileSetLoader tileSetLoader, ITemplateLoader templateLoader)
        {
            var structure = new DungeonStructure(Width, Heigth);

            var rectangleFiller = new RectangleFiller();
            rectangleFiller.Fill(structure, (int)WallType);
            structureLogger.LogStructure("Starting state", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var availableSpaceMap = new DungeonStructure(Width, Heigth);
            rectangleFiller.Fill(availableSpaceMap, 1);

            var random = RandomProvider.GetRandom();
            var shapeFactoryContext = new ShapeGeneratorFactoryContext(random);
            var shapeGenerators = ShapeGenerators.Select(x => (x.GetShapeGenerator(shapeFactoryContext), (double)x.Distribution));
            var randomFiller = new MultiShapeFiller(random, shapeGenerators);

            randomFiller.Fill(structure, ShapeCount, (int)FloorType);
            structureLogger.LogStructure("Random shape fill", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var regionMapExtractor = new SingleTypeRegionMapExtractor();
            var regionDataExtractor = new NormalRegionDataExtractor();
            var roomRegionMap = regionMapExtractor.Extract(structure, FloorType);
            var roomRegionData = regionDataExtractor.Extract(roomRegionMap);

            structureLogger.LogStructure("Room region map", StructureLogLevel.Medium, StructureType.RegionMap, roomRegionMap);

            var connector = new RandomLineRoomConnector(random, ConnectionTypes);
            connector.Connect(structure, FloorType, roomRegionData, roomRegionMap);

            structureLogger.LogStructure("Connected room region map", StructureLogLevel.Medium, StructureType.RegionMap, roomRegionMap);

            var corrosionConverter = new CorrosionConverter(CorrosionBufferInput, random);
            for (int i = 0; i < CorrosionIterationCount; i++)
            {
                corrosionConverter.Convert(structure, (int)FloorType, (int)WallType, CorrosionProbability);
                structureLogger.LogStructure("Corrosion", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);
            }

            for (int i = 0; i < SmoothIterationCount; i++)
            {
                var smoothConverter = new SmoothConverter(FloorType, WallType, SmoothBufferInput);
                smoothConverter.Convert(structure);
                structureLogger.LogStructure("Smoothed", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);
            }

            var roomFiller = new SmallRegionFiller();

            var floorRegionMap = regionMapExtractor.Extract(structure, FloorType);
            var floorRegionData = regionDataExtractor.Extract(floorRegionMap);
            roomFiller.Fill(structure, WallType, MinimumRoomSize, floorRegionData);

            structureLogger.LogStructure("Floor region map", StructureLogLevel.Medium, StructureType.RegionMap, floorRegionMap);
            structureLogger.LogStructure("Small rooms removed", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var wallRegionMap = regionMapExtractor.Extract(structure, WallType);
            var wallRegionData = regionDataExtractor.Extract(wallRegionMap);
            roomFiller.Fill(structure, FloorType, MinimumWallSize, wallRegionData);

            structureLogger.LogStructure("Wall region map", StructureLogLevel.Medium, StructureType.RegionMap, wallRegionMap);
            structureLogger.LogStructure("Small walls removed", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var finalRegionMap = regionMapExtractor.Extract(structure, FloorType);

            structureLogger.LogStructure("Room region map", StructureLogLevel.Medium, StructureType.RegionMap, finalRegionMap);
            structureLogger.LogStructure("Rooms connected", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            return structure;
        }
    }
}

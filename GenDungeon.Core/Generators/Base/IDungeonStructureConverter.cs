﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.Templates;
using GenDungeon.Core.TileSets;
using System.Collections.Generic;

namespace GenDungeon.Core.Generators
{
    public interface IDungeonGenerator
    {
        DungeonStructure Generate(IStructureLogger structureLogger, ITileSetLoader tileSetLoader, ITemplateLoader templateLoader);
    }
}

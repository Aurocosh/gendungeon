﻿using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.PerlinNoize;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.Templates;
using GenDungeon.Core.TileSets;
using GenDungeon.Core.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Generators
{
    public class PerlinDungeonGenerator : IDungeonGenerator
    {
        public int Width { get; set; }
        public int Heigth { get; set; }

        public float Threshold { get; set; }
        public TileType FloorType { get; set; }
        public TileType WallType { get; set; }

        public int MinimumRoomSize { get; set; }
        public int MinimumWallSize { get; set; }
        public int CorridorRadius { get; set; }

        public IRandomProvider RandomProvider { get; set; }
        public List<LineConnectionData> ConnectionTypes { get; set; }
        public List<PerlinNoizeGenerator> NoiseGenerators { get; set; }

        public DungeonStructure Generate(IStructureLogger structureLogger, ITileSetLoader tileSetLoader, ITemplateLoader templateLoader)
        {
            var structure = new DungeonStructure(Width, Heigth);
            structureLogger.LogStructure("Starting state", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var perlinNoiseConverter = new PerlinNoiseConverter()
            {
                Threshold = 0,
                EmptyType = FloorType,
                FilledType = WallType,
                NoiseGenerators = NoiseGenerators
            };
            perlinNoiseConverter.Convert(structure);
            structureLogger.LogStructure("Perlin noise", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var regionMapExtractor = new SingleTypeRegionMapExtractor();
            var regionDataExtractor = new NormalRegionDataExtractor();
            var regionFiller = new SmallRegionFiller();

            var floorRegionMap = regionMapExtractor.Extract(structure, FloorType);

            var floorRegionData = regionDataExtractor.Extract(floorRegionMap);
            regionFiller.Fill(structure, WallType, MinimumRoomSize, floorRegionData);
            var wallRegionMap = regionMapExtractor.Extract(structure, WallType);

            structureLogger.LogStructure("Floor map", StructureLogLevel.Medium, StructureType.RegionMap, floorRegionMap);
            structureLogger.LogStructure("Filled small rooms", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var wallRegionData = regionDataExtractor.Extract(wallRegionMap);
            regionFiller.Fill(structure, FloorType, MinimumWallSize, wallRegionData);

            structureLogger.LogStructure("Wall map", StructureLogLevel.Medium, StructureType.RegionMap, wallRegionMap);
            structureLogger.LogStructure("Removed small walls", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var roomRegionMap = regionMapExtractor.Extract(structure, FloorType);
            var roomRegionData = regionDataExtractor.Extract(roomRegionMap);

            structureLogger.LogStructure("Room region map", StructureLogLevel.Medium, StructureType.RegionMap, roomRegionMap);

            var random = RandomProvider.GetRandom();
            var connector = new RandomLineRoomConnector(random, ConnectionTypes);
            connector.Connect(structure, FloorType, roomRegionData, roomRegionMap);

            structureLogger.LogStructure("Connected room region map", StructureLogLevel.Medium, StructureType.RegionMap, roomRegionMap);
            structureLogger.LogStructure("Rooms connected", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            return structure;
        }
    }
}

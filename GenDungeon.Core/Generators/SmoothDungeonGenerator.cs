﻿using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.PerlinNoize;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.Templates;
using GenDungeon.Core.TileSets;
using GenDungeon.Core.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Generators
{
    public class SmoothDungeonGenerator : IDungeonGenerator
    {
        public int Width { get; set; }
        public int Heigth { get; set; }

        public TileType FloorType { get; set; }
        public TileType WallType { get; set; }

        public float FillPercent { get; set; }
        public bool BufferInput { get; set; }
        public int IterationCount { get; set; }

        public int MinimumRoomSize { get; set; }
        public int MinimumWallSize { get; set; }

        public IRandomProvider RandomProvider { get; set; }
        public List<LineConnectionData> ConnectionTypes { get; set; }

        public DungeonStructure Generate(IStructureLogger structureLogger, ITileSetLoader tileSetLoader, ITemplateLoader templateLoader)
        {
            var structure = new DungeonStructure(Width, Heigth);

            var rectangleFiller = new RectangleFiller();
            rectangleFiller.Fill(structure, (int)FloorType);
            structureLogger.LogStructure("Starting state", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var random = RandomProvider.GetRandom();
            var randomFiller = new SingleValueRandomFiller(random);
            randomFiller.Fill(structure, FillPercent, (int)WallType);
            structureLogger.LogStructure("Random fill", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            for (int i = 0; i < IterationCount; i++)
            {
                var smoothConverter = new SmoothConverter(FloorType, WallType, BufferInput);
                smoothConverter.Convert(structure);
                structureLogger.LogStructure("Smoothed", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);
            }

            var regionMapExtractor = new SingleTypeRegionMapExtractor();
            var regionDataExtractor = new NormalRegionDataExtractor();
            var roomFiller = new SmallRegionFiller();

            var floorRegionMap = regionMapExtractor.Extract(structure, FloorType);
            var floorRegionData = regionDataExtractor.Extract(floorRegionMap);
            roomFiller.Fill(structure, WallType, MinimumRoomSize, floorRegionData);

            structureLogger.LogStructure("Floor region map", StructureLogLevel.Medium, StructureType.RegionMap, floorRegionMap);
            structureLogger.LogStructure("Small rooms removed", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var wallRegionMap = regionMapExtractor.Extract(structure, WallType);
            var wallRegionData = regionDataExtractor.Extract(wallRegionMap);
            roomFiller.Fill(structure, FloorType, MinimumWallSize, wallRegionData);

            structureLogger.LogStructure("Wall region map", StructureLogLevel.Medium, StructureType.RegionMap, wallRegionMap);
            structureLogger.LogStructure("Small walls removed", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            var roomRegionMap = regionMapExtractor.Extract(structure, FloorType);
            var roomRegionData = regionDataExtractor.Extract(roomRegionMap);

            structureLogger.LogStructure("Room region map", StructureLogLevel.Medium, StructureType.RegionMap, roomRegionMap);

            var connector = new RandomLineRoomConnector(random, ConnectionTypes);
            connector.Connect(structure, FloorType, roomRegionData, roomRegionMap);

            structureLogger.LogStructure("Connected room region map", StructureLogLevel.Medium, StructureType.RegionMap, roomRegionMap);
            structureLogger.LogStructure("Rooms connected", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            return structure;
        }
    }
}

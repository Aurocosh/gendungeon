﻿using GenDungeon.Core.Data;
using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.InputData;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.PatternExtractors;
using GenDungeon.Core.PerlinNoize;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Core.ShapeProvider.Base;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.Templates;
using GenDungeon.Core.TileSets;
using GenDungeon.Core.TileSets.Painter;
using GenDungeon.Core.Tools;
using GenDungeon.Core.Tools.Pattern;
using GenDungeon.Core.Tools.WaveFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Generators
{
    public class PatternDungeonGenerator : IDungeonGenerator
    {
        public int Width { get; set; }
        public int Heigth { get; set; }

        public string Template { get; set; }
        public TileType UndefinedTile { get; set; }
        public TileType OutOfBoundsTile { get; set; }

        public bool FixExceptions { get; set; }
        public bool IgnoreExceptions { get; set; }

        public CoordinateWrapping InputWrapping { get; set; }
        public CoordinateWrapping OutputWrapping { get; set; }

        public List<TemplateTransformationData> TemplateTransformations { get; set; }

        public IShapeProvider ShapeProvider { get; set; }
        public IRandomProvider RandomProvider { get; set; }

        public DungeonStructure Generate(IStructureLogger structureLogger, ITileSetLoader tileSetLoader, ITemplateLoader templateLoader)
        {
            var structure = new DungeonStructure(Width, Heigth);
            var template = templateLoader.LoadTemplate(Template);

            //var patternShifts = Vector2IntConst.EightDirections.ToList();
            //var patternShifts = new SolidCircle(Vector2Int.Zero, 3).ToHashSet();
            //patternShifts.Remove(Vector2Int.Zero);
            var shape = ShapeProvider.GetShape();
            var patternShifts = shape.ToHashSet();
            patternShifts.Remove(Vector2Int.Zero);

            var random = RandomProvider.GetRandom();
            var transformations = TemplateTransformations.Select(x => x.GetData());
            var waveFunctionFiller = new PatternFiller(random, template, (int)OutOfBoundsTile, (int)UndefinedTile, IgnoreExceptions, InputWrapping, OutputWrapping, patternShifts, transformations);
            waveFunctionFiller.Fill(structure, structureLogger);
            structureLogger.LogStructure("Generated", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            if (FixExceptions)
            {
                var neighbourFiller = new NeighbourTileTypeFiller(random, Vector2IntConst.FourDirections);
                neighbourFiller.Fill(structure, (int)UndefinedTile);
                structureLogger.LogStructure("Fixed", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);
            }

            return structure;
        }
    }
}

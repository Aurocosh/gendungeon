﻿using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.PerlinNoize;
using GenDungeon.Core.RandomProvider.Base;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.Templates;
using GenDungeon.Core.TileSets;
using GenDungeon.Core.TileSets.Painter;
using GenDungeon.Core.Tools;
using GenDungeon.Core.Tools.WaveFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Generators
{
    public class WaveFunctionDungeonGenerator : IDungeonGenerator
    {
        public int Width { get; set; }
        public int Heigth { get; set; }

        public string Template { get; set; }

        public int N { get; set; }
        public int Ground { get; set; }
        public int Symmetry { get; set; }
        public bool PeriodicInput { get; set; }

        public int Limit { get; set; }
        public bool Periodic { get; set; }

        public IRandomProvider RandomProvider { get; set; }

        public DungeonStructure Generate(IStructureLogger structureLogger, ITileSetLoader tileSetLoader, ITemplateLoader templateLoader)
        {
            var structure = new DungeonStructure(Width, Heigth);
            var template = templateLoader.LoadTemplate(Template);

            var random = RandomProvider.GetRandom();
            var waveFunctionFiller = new WaveFunctionOverlappingFiller(random);
            waveFunctionFiller.Fill(structure, template, N, Symmetry, Ground, PeriodicInput, Periodic, Limit);
            structureLogger.LogStructure("Tile set painted", StructureLogLevel.Medium, StructureType.DungeonStructure, structure);

            return structure;
        }
    }
}

﻿using GenDungeon.Core.Dungeon.Converter;
using GenDungeon.Core.InputData;
using GenDungeon.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Data
{
    public class TemplateTransformationData : ISerializableContainer<TemplateTransformation>
    {
        public StructureMirror Mirror { get; set; }
        public List<StructureRotation> Rotations { get; set; }

        public TemplateTransformation GetData()
        {
            return new TemplateTransformation(Mirror, Rotations);
        }
    }
}

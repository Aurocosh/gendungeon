﻿using GenDungeon.Core.Data;
using GenDungeon.Core.Interfaces;
using GenDungeon.Core.Tools.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.DataSerialization
{
    public class LineConnectionData : ISerializableContainer<LineConnection>
    {
        public float Distribution { get; set; }
        public int ConnectionThickness { get; set; }
        public LineConnectionType ConnectionType { get; set; }

        public LineConnection GetData()
        {
            return new LineConnection(ConnectionThickness, ConnectionType);
        }
    }
}

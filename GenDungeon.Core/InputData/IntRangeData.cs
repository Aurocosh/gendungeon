﻿using GenDungeon.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Data
{
    public class IntRangeData : ISerializableContainer<IntRange>
    {
        public int Min { get; set; }
        public int Max { get; set; }

        public IntRange GetData()
        {
            return new IntRange(Min, Max);
        }
    }
}

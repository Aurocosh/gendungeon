﻿using GenDungeon.Core.Data.Tiles;
using GenDungeon.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.InputData.Tiles
{
    public class TileSetConfigData : ISerializableContainer<TileSetConfig>
    {
        public int CellSize { get; set; }
        public string BorderConnection { get; set; }
        public List<string> ConnectionTypes { get; set; }

        public TileSetConfig GetData()
        {
            return new TileSetConfig(CellSize, BorderConnection, ConnectionTypes);
        }
    }
}

﻿using GenDungeon.Core.Data.Tiles;
using GenDungeon.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.InputData.Tiles
{
    public class TileConfigData : ISerializableContainer<TileConfig>
    {
        public List<string> ConnectionsUp { get; set; }
        public List<string> ConnectionsRight { get; set; }
        public List<string> ConnectionsDown { get; set; }
        public List<string> ConnectionsLeft { get; set; }
        public List<StructureRotation> Rotations { get; set; }

        public TileConfig GetData()
        {
            return new TileConfig(ConnectionsUp, ConnectionsRight, ConnectionsDown, ConnectionsLeft, Rotations);
        }
    }
}

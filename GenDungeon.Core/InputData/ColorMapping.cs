﻿using GenDungeon.Core.Dungeon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.InputData.BitmapDungeonMapper
{
    public class ColorMapping
    {
        public Dictionary<string, TileType> Mapping { get; set; }

        public Dictionary<string, int> ToIntMapping()
        {
            return Mapping.ToDictionary(x => x.Key, x => (int)x.Value);
        }
    }
}

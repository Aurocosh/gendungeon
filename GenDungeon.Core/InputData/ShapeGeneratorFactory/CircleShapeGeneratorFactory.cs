﻿using GenDungeon.Core.Data;
using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.DataSerialization.ShapeGeneratorFactory;
using GenDungeon.Core.Lib;
using GenDungeon.Core.ShapeGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Generators
{
    public class CircleShapeGeneratorFactory : IShapeGeneratorFactory
    {
        public bool CanOverlap { get; set; }
        public float Distribution { get; set; }
        public float ExtraAreaPercent { get; set; }
        public IntRangeData RadiusRange { get; set; }

        public IShapeGenerator GetShapeGenerator(ShapeGeneratorFactoryContext context)
        {
            return new CircleShapeGenerator(context.Random, RadiusRange.GetData(), ExtraAreaPercent, CanOverlap);
        }
    }
}

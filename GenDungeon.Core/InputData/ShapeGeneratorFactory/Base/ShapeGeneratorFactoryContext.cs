﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.DataSerialization.ShapeGeneratorFactory
{
    public class ShapeGeneratorFactoryContext
    {
        public Random Random { get; }

        public ShapeGeneratorFactoryContext(Random random)
        {
            Random = random;
        }
    }
}

﻿using GenDungeon.Core.DataSerialization.ShapeGeneratorFactory;
using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.DataSerialization
{
    public interface IShapeGeneratorFactory
    {
        float Distribution { get; }
        IShapeGenerator GetShapeGenerator(ShapeGeneratorFactoryContext context);
    }
}

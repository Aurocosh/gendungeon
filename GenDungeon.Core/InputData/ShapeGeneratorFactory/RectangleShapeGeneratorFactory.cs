﻿using GenDungeon.Core.Data;
using GenDungeon.Core.DataSerialization.ShapeGeneratorFactory;
using GenDungeon.Core.ShapeGenerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.DataSerialization
{
    public class RectangleShapeGeneratorFactory : IShapeGeneratorFactory
    {
        public bool CanOverlap { get; set; }
        public float Distribution { get; set; }
        public float ExtraAreaPercent { get; set; }
        public IntRangeData WidthRange { get; set; }
        public IntRangeData HeightRange { get; set; }

        public IShapeGenerator GetShapeGenerator(ShapeGeneratorFactoryContext context)
        {
            return new RectangleShapeGenerator(context.Random, WidthRange.GetData(), HeightRange.GetData(), ExtraAreaPercent, CanOverlap);
        }
    }
}

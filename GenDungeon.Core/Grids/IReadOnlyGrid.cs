﻿using GenDungeon.Core.Interfaces;
using GenDungeon.Core.Lib;
using GenDungeon.Core.StructureLogger;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Grids
{
    public interface IReadOnlyGrid<T>
    {
        int Width { get; }
        int Height { get; }
        T this[int x, int y] { get; }
        T this[Vector2Int point] { get; }

        Rectangle Rectangle { get; }

        bool IsItInBounds(int x, int y);
        bool IsItInBounds(Vector2Int point);
    }
}

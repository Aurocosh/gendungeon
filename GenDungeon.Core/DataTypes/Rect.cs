﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.DataTypes
{
    public class Rect
    {
        public int Y { get; set; }
        public int X { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public Rect()
        {
            Y = 0;
            X = 0;
            Width = 0;
            Height = 0;
        }

        public Rect(int y, int x, int width, int height)
        {
            Y = y;
            X = x;
            Width = width;
            Height = height;
        }

        public Rectangle ToRectangle() { return new Rectangle(X, Y, Width, Height); }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class CorrosionConverter
    {
        public bool BufferInput { get; }
        public Random Random { get; }

        public CorrosionConverter(bool bufferInput, Random random)
        {
            BufferInput = bufferInput;
            Random = random;
        }

        public void Convert(DungeonStructure structure, int emptyType, int filledType, float corrosionProbability)
        {
            Convert(structure, emptyType, filledType, corrosionProbability, structure.Rectangle);
        }

        public void Convert(DungeonStructure structure, int emptyType, int filledType, float corrosionProbability, Rectangle rectangle)
        {
            var input = BufferInput ? structure.DeepCopy() : structure;
            foreach (var coordinate in new SolidRectangle(rectangle).ToShuffledList(Random))
            {
                if (input[coordinate] == filledType)
                {
                    var filledNeighbours = GetFilledCount(input, coordinate, filledType);
                    if (filledNeighbours > 0 && filledNeighbours < 8 && RandomUtility.RollDice(corrosionProbability, Random))
                        structure[coordinate] = emptyType;
                }
            }
        }

        private static int GetFilledCount(DungeonStructure structure, Vector2Int coordinate, int filledType)
        {
            int filledCount = 0;
            foreach (var neighbour in Vector2IntConst.EightDirections.Select(x => x + coordinate))
            {
                if (structure.IsItInBounds(neighbour))
                {
                    int tileType = structure[neighbour];
                    if (tileType == filledType)
                        filledCount++;
                }
                else
                {
                    filledCount++;
                }
            }

            return filledCount;
        }
    }
}

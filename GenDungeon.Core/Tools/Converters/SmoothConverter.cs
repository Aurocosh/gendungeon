﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class SmoothConverter
    {
        public bool BufferInput { get; }
        public TileType EmptyType { get; }
        public TileType FilledType { get; }

        public SmoothConverter(TileType emptyType, TileType filledType, bool bufferInput)
        {
            EmptyType = emptyType;
            FilledType = filledType;
            BufferInput = bufferInput;
        }

        public void Convert(DungeonStructure structure)
        {
            Convert(structure, new Rectangle(0, 0, structure.Width, structure.Height));
        }

        public void Convert(DungeonStructure structure, Rectangle rectangle)
        {
            var input = BufferInput ? structure.DeepCopy() : structure;
            foreach (var coordinate in new SolidRectangle(rectangle))
            {
                var neighbourWallTiles = GetSurroundingWallCount(coordinate, input);
                if (neighbourWallTiles > 4)
                    structure[coordinate] = (int)FilledType;
                else if (neighbourWallTiles < 4)
                    structure[coordinate] = (int)EmptyType;
            }
        }

        private int GetSurroundingWallCount(Vector2Int coordinate, DungeonStructure structure)
        {
            var wallCount = 0;
            foreach (var neighbour in Vector2IntConst.EightDirections.Select(x => x + coordinate))
            {
                if (structure.IsItInBounds(neighbour))
                {
                    var isItWall = structure[neighbour] == (int)FilledType ? 1 : 0;
                    wallCount += isItWall;
                }
                else
                {
                    wallCount++;
                }
            }

            return wallCount;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.PerlinNoize;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class PerlinNoiseConverter
    {
        public float Threshold { get; set; }
        public List<PerlinNoizeGenerator> NoiseGenerators { get; set; }

        public TileType EmptyType { get; set; }
        public TileType FilledType { get; set; }

        public PerlinNoiseConverter()
        {
            Threshold = 0;
            NoiseGenerators = new List<PerlinNoizeGenerator>();
            EmptyType = TileType.Door;
            FilledType = TileType.Wall;
        }

        public void Convert(DungeonStructure structure)
        {
            Convert(structure, new Rectangle(0, 0, structure.Width, structure.Height));
        }

        public void Convert(DungeonStructure structure, Rectangle rectangle)
        {
            foreach (var coordinate in new SolidRectangle(rectangle))
            {
                var noise = NoiseGenerators.Select(x => x.Noise(coordinate)).Sum();
                var tile = noise < Threshold ? FilledType : EmptyType;
                structure[coordinate] = (int)tile;
            }
        }
    }
}

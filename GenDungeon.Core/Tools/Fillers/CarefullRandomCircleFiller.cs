﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class CarefullRandomCircleFiller
    {
        public Random Random { get; set; }

        public CarefullRandomCircleFiller()
        {
            Random = new Random();
        }

        public CarefullRandomCircleFiller(Random random)
        {
            Random = random;
        }

        public void Fill(DungeonStructure structure, int circleCount, int minRadius, int maxRadius, TileType tileType)
        {
            Fill(structure, circleCount, minRadius, maxRadius, tileType, new Rectangle(0, 0, structure.Width, structure.Height));
        }

        public void Fill(DungeonStructure structure, int circleCount, int minRadius, int maxRadius, TileType tileType, Rectangle rectangle)
        {
            var solidRectangle = new SolidRectangle(rectangle).ToShuffledList(Random);
            var coordinateOrder = new Queue<Vector2Int>(solidRectangle);
            var coordinateSet = new HashSet<Vector2Int>(coordinateOrder);

            int circlesDrawn = 0;
            while (circlesDrawn < circleCount && coordinateSet.Count > 0)
            {
                var nextCoordinate = coordinateOrder.Dequeue();
                if (coordinateSet.Contains(nextCoordinate))
                {
                    int circleRadius = Random.Next(minRadius, maxRadius + 1);
                    var circle = new SolidCircle(nextCoordinate, circleRadius);
                    var coordinates = circle.Where(x => structure.IsItInBounds(x)).ToList();
                    DungeonStructureDrawing.DrawPointCloud(structure, coordinates, (int)tileType);
                    coordinateSet.ExceptWith(coordinates);

                    circlesDrawn++;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class MultiValueRandomShapeFiller
    {
        public Random Random { get; set; }

        public MultiValueRandomShapeFiller()
        {
            Random = new Random();
        }

        public MultiValueRandomShapeFiller(Random random)
        {
            Random = random;
        }

        public void Fill(DungeonStructure structure, IEnumerable<int> possibleValues)
        {
            Fill(structure, possibleValues, new SolidRectangle(structure.Rectangle));
        }

        public void Fill(DungeonStructure structure, IEnumerable<int> possibleValues, IEnumerable<Vector2Int> coordinates)
        {
            var values = possibleValues.ToList();
            foreach (var coordinate in coordinates)
                structure[coordinate] = values.GetRandom(Random);
        }
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using GenDungeon.Core.ValueExtractors;
using GenDungeon.Core.ValueExtractors.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Tools
{
    public class NeighbourTileTypeFiller
    {
        private readonly Random _random;
        private readonly IValueExtractor _valueExtractor;

        public NeighbourTileTypeFiller(Random random, IEnumerable<Vector2Int> shapeShifts)
        {
            _random = random;
            var shifts = shapeShifts.ToHashSet();
            shifts.Remove(Vector2Int.Zero);
            _valueExtractor = new GenericValueExtractor(shifts);
        }

        public void Fill(DungeonStructure structure, int tileType)
        {
            Convert(structure, tileType, new SolidRectangle(structure.Rectangle));
        }

        public void Convert(DungeonStructure structure, int tileType, IEnumerable<Vector2Int> shapeCoordinates)
        {
            var delayed = new Queue<Vector2Int>();
            foreach (var coordinate in shapeCoordinates)
            {
                if (!FillTile(structure, tileType, coordinate))
                {
                    delayed.Enqueue(coordinate);
                }
            }

            int limit = structure.Width * structure.Height;
            while (delayed.Count > 0 && limit-- > 0)
            {
                var coordinate = delayed.Dequeue();
                if (!FillTile(structure, tileType, coordinate))
                {
                    delayed.Enqueue(coordinate);
                }
            }
        }

        private bool FillTile(DungeonStructure structure, int tileType, Vector2Int coordinate)
        {
            int type = structure[coordinate];
            if (type == tileType)
            {
                var neighbourValues = _valueExtractor.ExtractValues(structure, coordinate);
                var valueCount = IEnumerableUtils.CountDistinctItems(neighbourValues);
                valueCount.Remove(tileType);
                if (valueCount.Count == 0)
                {
                    return false;
                }
                else
                {
                    int maxFrequency = valueCount.Max(x => x.Value);
                    var mostFrequent = valueCount.Where(x => x.Value == maxFrequency).Select(x => x.Key).ToList();
                    structure[coordinate] = mostFrequent.GetRandom(_random);
                    return true;
                }
            }
            return true;
        }
    }
}

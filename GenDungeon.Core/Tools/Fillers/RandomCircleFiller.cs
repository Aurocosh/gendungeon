﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class RandomCircleFiller
    {
        public Random Random { get; set; }

        public RandomCircleFiller()
        {
            Random = new Random();
        }

        public RandomCircleFiller(Random random)
        {
            Random = random;
        }

        public void Fill(DungeonStructure structure, int circleCount, int minRadius, int maxRadius, TileType tileType)
        {
            Fill(structure, circleCount, minRadius, maxRadius, tileType, new Rectangle(0, 0, structure.Width, structure.Height));
        }

        public void Fill(DungeonStructure structure, int circleCount, int minRadius, int maxRadius, TileType tileType, Rectangle rectangle)
        {
            for (int i = 0; i < circleCount; i++)
            {
                int x = rectangle.X + Random.Next(rectangle.Width);
                int y = rectangle.Y + Random.Next(rectangle.Height);

                int circleRadius = Random.Next(minRadius, maxRadius + 1);
                var circle = new SolidCircle(x, y, circleRadius);
                DungeonStructureDrawing.DrawPointCloud(structure, circle, (int)tileType);
            }
        }
    }
}

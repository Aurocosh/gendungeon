﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class MultiShapeFiller
    {
        private readonly Random _random;
        private readonly DistributedRandomList<IShapeGenerator> _shapeGenerators;

        public MultiShapeFiller(Random random, IEnumerable<(IShapeGenerator, double)> shapeGenerators)
        {
            _random = random;
            _shapeGenerators = new DistributedRandomList<IShapeGenerator>(random, shapeGenerators);
        }

        public void Fill(DungeonStructure structure, int shapeCount, int tileType)
        {
            Fill(structure, shapeCount, tileType, structure.Rectangle);
        }

        public void Fill(DungeonStructure structure, int shapeCount, int tileType, Rectangle rectangle)
        {
            var solidRectangle = new SolidRectangle(rectangle).ToShuffledList(_random);
            var coordinateOrder = new Queue<Vector2Int>(solidRectangle);
            var usedCoordinatesSet = new HashSet<Vector2Int>(coordinateOrder.Count);

            int shapesDrawn = 0;
            while (shapesDrawn < shapeCount && coordinateOrder.Count > 0)
            {
                var nextCoordinate = coordinateOrder.Dequeue();
                var shapeGenerator = _shapeGenerators.GetRandom();
                var (shapePointsIterator, areaPointsIterator) = shapeGenerator.GetShapeAndAreaPoints(nextCoordinate);
                var shapePoints = shapePointsIterator.Where(x => structure.IsItInBounds(x)).ToList();
                if (shapeGenerator.CanOverlap || !usedCoordinatesSet.Overlaps(shapePoints))
                {
                    DungeonStructureDrawing.DrawPointCloud(structure, shapePoints, tileType);

                    var areaPoints = areaPointsIterator.Where(x => structure.IsItInBounds(x));
                    usedCoordinatesSet.UnionWith(areaPoints);

                    shapesDrawn++;
                }
            }
        }
    }
}

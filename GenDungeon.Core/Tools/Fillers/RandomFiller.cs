﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class SingleValueRandomFiller
    {
        public Random Random { get; set; }

        public SingleValueRandomFiller()
        {
            Random = new Random();
        }

        public SingleValueRandomFiller(Random random)
        {
            Random = random;
        }

        public void Fill(DungeonStructure structure, float fillPercent, int tileType)
        {
            Convert(structure, fillPercent, tileType, new SolidRectangle(structure.Rectangle));
        }

        public void Convert(DungeonStructure structure, float fillPercent, int tileType, IEnumerable<Vector2Int> shapeCoordinates)
        {
            foreach (var coordinate in shapeCoordinates)
            {
                if (RandomUtility.RollDice(fillPercent, Random))
                {
                    structure[coordinate] = tileType;
                }
            }
        }
    }
}

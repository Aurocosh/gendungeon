﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class SmallRegionFiller
    {
        public void Fill(DungeonStructure structure, TileType tileType, int minimumSize, RegionData regionData)
        {
            int type = (int)tileType;
            foreach (var pair in regionData.RegionCoordinates)
            {
                var coordinates = pair.Value;
                if (coordinates.Count < minimumSize)
                {
                    foreach (var coordinate in coordinates)
                    {
                        structure[coordinate] = type;
                    }
                }
            }
        }
    }
}

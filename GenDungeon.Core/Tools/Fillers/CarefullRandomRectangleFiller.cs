﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.Data;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class CarefullRandomRectangleFiller
    {
        public int AttemptLimit { get; set; }
        public Random Random { get; set; }

        public CarefullRandomRectangleFiller()
        {
            AttemptLimit = 300;
            Random = new Random();
        }

        public CarefullRandomRectangleFiller(int attemptLimit, Random random)
        {
            AttemptLimit = attemptLimit;
            Random = random;
        }

        public void Fill(DungeonStructure structure, int rectangleCount, int borderSize, IntRange widthRange, IntRange heightRange, int tileType, DungeonStructure availableSpaceMap)
        {
            Fill(structure, rectangleCount, borderSize, widthRange, heightRange, tileType, availableSpaceMap, structure.Rectangle);
        }

        public void Fill(DungeonStructure structure, int rectangleCount, int borderSize, IntRange widthRange, IntRange heightRange, int tileType, DungeonStructure availableSpaceMap, Rectangle rectangle)
        {
            var xRange = rectangle.GetRangeX();
            var yRange = rectangle.GetRangeY();

            int attempts = AttemptLimit;
            int rectanglesPlaced = 0;
            while (rectanglesPlaced < rectangleCount && attempts-- > 0)
            {
                int rectX = Random.NextInclusive(xRange);
                int rectY = Random.NextInclusive(yRange);

                int width = Random.NextInclusive(widthRange);
                int heigth = Random.NextInclusive(heightRange);

                var soliRectangle = new SolidRectangle(rectX, rectY, width, heigth);
                var spaceIsAvailable = soliRectangle.All(x => availableSpaceMap.IsItInBounds(x) && availableSpaceMap[x] == 1);
                if (spaceIsAvailable)
                {
                    rectanglesPlaced++;
                    DungeonStructureDrawing.DrawPointCloud(structure, soliRectangle, tileType);

                    var claimedSpace = new Rectangle(rectX - borderSize, rectY - borderSize, width + borderSize * 2, heigth + borderSize * 2);
                    var claimedSpaceWalker = new SolidRectangle(claimedSpace);
                    DungeonStructureDrawing.DrawPointCloudSafe(availableSpaceMap, claimedSpaceWalker, 0);
                }
            }
        }
    }
}

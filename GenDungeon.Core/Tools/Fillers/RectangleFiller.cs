﻿using System.Collections.Generic;
using System.Drawing;
using GenDungeon.Core.Dungeon;

namespace GenDungeon.Core.Tools
{
    public class RectangleFiller
    {
        public void Fill(DungeonStructure structure, int tileType)
        {
            Fill(structure, tileType, new Rectangle(0, 0, structure.Width, structure.Height));
        }

        public void Fill(DungeonStructure structure, int tileType, Rectangle rectangle)
        {
            for (int x = 0; x < rectangle.Width; x++)
            {
                for (int y = 0; y < rectangle.Height; y++)
                    structure[rectangle.X + x, rectangle.Y + y] = tileType;
            }
        }
    }
}

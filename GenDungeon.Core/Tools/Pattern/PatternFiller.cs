﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNet.Collections.Generic;
using GenDungeon.Core.Data;
using GenDungeon.Core.Data.Tiles;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Dungeon.Converter;
using GenDungeon.Core.Grids;
using GenDungeon.Core.InputData;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Lib.Extensions;
using GenDungeon.Core.PatternExtractors;
using GenDungeon.Core.PatternExtractors.Base;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.StructureLogger;
using GenDungeon.Core.TileSets;
using GenDungeon.Core.TileSets.Tiles;
using GenDungeon.Core.Tools.Tiles;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;
using Priority_Queue;

namespace GenDungeon.Core.Tools.Pattern
{
    public class PatternFiller
    {
        private readonly Random _random;
        private readonly IReadOnlyDungeonStructure _template;
        private readonly IEnumerable<Vector2Int> _patternCoordinateShifts;

        private readonly List<int> _templateOriginalValues;
        private readonly List<int> _templateAdaptedValues;

        private readonly bool _ignoreErrors;

        private readonly int _emptyTileId;
        private readonly int _firstUndefinedStateId;

        private readonly int _emptyStateId;
        private readonly int _unknownStateId;
        private readonly int _unknownStateValueCount;

        private readonly int _minPartialMatch;

        private readonly List<HashSet<int>> _possibleStates;
        private readonly List<int> _possibleStateIds;
        private readonly List<int> _possibleStateValuesCount;

        private readonly Dictionary<int, int> _determinedStatesMap;

        private readonly Dictionary<ValuePattern, int> _valuePatternMap;
        private readonly Dictionary<StatePattern, int> _statePatternMap;
        private readonly Dictionary<StatePattern, int> _partialStatePatternMap;

        private readonly IPatternExtractor _valuePatternExtractor;
        private readonly IPatternExtractor _statePatternExtractor;

        public PatternFiller(Random random, IReadOnlyDungeonStructure template, int outOfBoundsValue, int undefinedTileId, bool ignoreErrors, CoordinateWrapping inputWrapping, CoordinateWrapping outputWrapping, IEnumerable<Vector2Int> patternCoordinateShifts, IEnumerable<TemplateTransformation> transformations)
        {
            _random = random;
            _template = template;
            _ignoreErrors = ignoreErrors;
            _emptyTileId = undefinedTileId;

            var shiftSet = patternCoordinateShifts.ToHashSet();
            shiftSet.Remove(Vector2Int.Zero);
            _patternCoordinateShifts = shiftSet.OrderBy(x => x.LengthSquared()).ToList();

            var templateRect = new SolidRectangle(template.Rectangle);

            _templateOriginalValues = templateRect
                .Select(x => template[x])
                .Append(outOfBoundsValue)
                .Distinct()
                .ToList();

            var templateAdaptationMap = _templateOriginalValues
                .Select((v, i) => (v, i))
                .ToDictionary(x => x.v, x => x.i);

            _templateAdaptedValues = Enumerable.Range(0, _templateOriginalValues.Count).ToList();

            _template = template;
            var adaptedTemplate = new DungeonStructure(template.Width, template.Height);
            foreach (var coordinate in templateRect)
            {
                var value = _template[coordinate];
                adaptedTemplate[coordinate] = templateAdaptationMap[value];
            }

            var stateGenerator = new StateGenerator();
            _possibleStates = stateGenerator.GenerateStates(_templateAdaptedValues).Select(x => x.ToHashSet()).ToList();
            _possibleStateIds = Enumerable.Range(0, _possibleStates.Count).ToList();
            _possibleStateValuesCount = _possibleStates.Select(x => x.Count).ToList();

            _firstUndefinedStateId = _possibleStateIds.First(x => _possibleStates[x].Count > 1);

            _emptyStateId = 0;
            _unknownStateId = _possibleStateIds.Count - 1;
            _unknownStateValueCount = _possibleStateValuesCount[_unknownStateId];

            _minPartialMatch = (shiftSet.Count / 3) * 2 + 1;

            var adaptedOutOfBoundsValue = templateAdaptationMap[outOfBoundsValue];
            //var outOfBoundsStateId = FindStateId(new HashSet<int> { adaptedOutOfBoundsValue });
            var outOfBoundsStateId = _unknownStateId;

            _determinedStatesMap = _templateAdaptedValues.ToDictionary(x => x, x => FindStateId(new HashSet<int>() { x }));

            var extractorShifts = _patternCoordinateShifts.Append(Vector2Int.Zero).ToList();
            _valuePatternExtractor = PatternExtractorFactory.GetPatternExtractor(inputWrapping, adaptedOutOfBoundsValue, extractorShifts);
            _statePatternExtractor = PatternExtractorFactory.GetPatternExtractor(outputWrapping, outOfBoundsStateId, extractorShifts);

            _valuePatternMap = new Dictionary<ValuePattern, int>();

            foreach (var transformation in transformations)
            {
                var mirroredStructure = DungeonStructureReflector.Mirror(adaptedTemplate, transformation.Mirror);
                foreach (var rotation in transformation.Rotations)
                {
                    var rotatedAdaptedTemplate = DungeonStructureRotator.Rotate(mirroredStructure, rotation);
                    foreach (var coordinate in new SolidRectangle(rotatedAdaptedTemplate.Rectangle))
                    {
                        var value = rotatedAdaptedTemplate[coordinate];
                        var patternElements = _valuePatternExtractor.ExtractPattern(rotatedAdaptedTemplate, coordinate);
                        var valuePattern = new ValuePattern(patternElements);
                        _valuePatternMap[valuePattern] = value;
                    }
                }
            }

            _statePatternMap = new Dictionary<StatePattern, int>();
            _partialStatePatternMap = new Dictionary<StatePattern, int>();
        }

        private int FindStateId(HashSet<int> stateValues)
        {
            foreach (int id in _possibleStateIds)
            {
                var state = _possibleStates[id];
                if (state.SetEquals(stateValues))
                    return id;
            }
            return -1;
        }

        private int GetState(StatePattern pattern)
        {
            if (_statePatternMap.TryGetValue(pattern, out var state))
                return state;

            var stateValues = new HashSet<int>();
            var multiPattern = pattern.Elements.Select(x => _ignoreErrors && x == 0 ? _possibleStates[_unknownStateId] : _possibleStates[x]);
            var multiPatternMatcher = new MultiPatternMatcher(multiPattern);
            foreach (var pair in _valuePatternMap)
            {
                if (multiPatternMatcher.IsMatching(pair.Key))
                    stateValues.Add(pair.Value);
            }

            int stateId = FindStateId(stateValues);
            _statePatternMap[pattern] = stateId;
            return stateId;
        }

        private int GetPartialState(StatePattern pattern)
        {
            if (_partialStatePatternMap.TryGetValue(pattern, out var state))
                return state;

            var multiPattern = pattern.Elements.Select(x => _ignoreErrors && x == 0 ? _possibleStates[_unknownStateId] : _possibleStates[x]);
            var multiPatternMatcher = new PartialMultiPatternMatcher(multiPattern);

            int maxMatchCount = 0;
            int value = _emptyTileId;
            foreach (var pair in _valuePatternMap)
            {
                int matchCount = multiPatternMatcher.CountMatching(pair.Key);
                if (matchCount > maxMatchCount)
                {
                    value = pair.Value;
                    maxMatchCount = matchCount;
                }
            }

            int stateId = FindStateId(new HashSet<int> { value });
            _partialStatePatternMap[pattern] = stateId;
            return stateId;
        }


        public void Fill(DungeonStructure structure, IStructureLogger structureLogger)
        {
            Fill(structure, new SolidRectangle(structure.Rectangle), structureLogger);
        }

        public void Fill(DungeonStructure structure, ICoordinateShape shape, StructureLogger.IStructureLogger structureLogger)
        {
            int regionSize = 50;

            var queueGrid = new Grid<int>(structure.Width, structure.Height);
            var stateGrid = new DungeonStructure(structure.Width, structure.Height);

            var stateGridRect = new SolidRectangle(stateGrid.Rectangle);
            foreach (var coordinate in stateGridRect)
            {
                stateGrid[coordinate] = _unknownStateId;
                queueGrid[coordinate] = -1;
            }

            int tilesProcessed = 0;
            var priorityQueue = new SimplePriorityQueue<Vector2Int, int>();
            var regions = new SectionedRectangle(stateGrid.Rectangle, regionSize, regionSize);
            foreach (var region in regions.IterateSections())
            {
                foreach (var unexplored in region)
                {
                    int unexploredStateId = stateGrid[unexplored];
                    if (unexploredStateId >= _firstUndefinedStateId)
                    {
                        int stateValueCount = _possibleStateValuesCount[unexploredStateId];
                        priorityQueue.Enqueue(unexplored, stateValueCount);
                        queueGrid[unexplored] = stateValueCount;
                    }

                    while (priorityQueue.Count > 0)
                    {
                        var coordinate = priorityQueue.Dequeue();
                        int stateId = stateGrid[coordinate];
                        if (stateId < _firstUndefinedStateId)
                        {
                            queueGrid[coordinate] = -1;
                            continue;
                        }

                        bool cellChanged = false;
                        var patternElements = _statePatternExtractor.ExtractPattern(stateGrid, coordinate);
                        var pattern = new StatePattern(patternElements);
                        var newStateId = GetState(pattern);

                        if (stateId == newStateId)
                        {
                            int priority = queueGrid[coordinate];
                            if (priority > _unknownStateValueCount)
                            {
                                var possibleTileValues = _possibleStates[stateId];
                                var selectedTileId = possibleTileValues.ToList().GetRandom(_random);
                                stateGrid[coordinate] = _determinedStatesMap[selectedTileId];
                                queueGrid[coordinate] = -1;
                                cellChanged = true;
                            }
                            else if (priority > -1)
                            {
                                int newStateValueCount = _possibleStateValuesCount[newStateId] + _unknownStateValueCount + 1;
                                priorityQueue.Enqueue(coordinate, newStateValueCount);
                                queueGrid[coordinate] = newStateValueCount;
                            }
                        }
                        else
                        {
                            stateGrid[coordinate] = newStateId;
                            queueGrid[coordinate] = -1;
                            cellChanged = true;
                        }

                        if (cellChanged)
                        {
                            //structureLogger.AddPartialState("Gen", StructureLogLevel.Medium, StructureType.RegionMap, stateGrid);
                            foreach (var nextCoordinate in _patternCoordinateShifts.Select(x => x + coordinate).Where(x => region.IsInBounds(x)))
                            {
                                int state = stateGrid[nextCoordinate];
                                if (state >= _firstUndefinedStateId)
                                {
                                    int priority = queueGrid[nextCoordinate];
                                    int stateCount = _possibleStateValuesCount[state];
                                    if (priority <= -1 || priority > _unknownStateValueCount || stateCount < priority)
                                    {
                                        priorityQueue.Enqueue(nextCoordinate, stateCount);
                                        queueGrid[nextCoordinate] = stateCount;
                                    }
                                }
                            }
                            tilesProcessed++;
                        }
                    }
                }
            }

            //structureLogger.LogPartialStructure("Gen", StructureLogLevel.Medium, StructureType.RegionMap, stateGrid);
            structureLogger.LogStructure("Gen", StructureLogLevel.Medium, StructureType.RegionMap, stateGrid);

            foreach (var coordinate in stateGridRect)
            {
                int stateId = stateGrid[coordinate];
                if (stateId == _emptyStateId)
                {
                    var patternElements = _statePatternExtractor.ExtractPattern(stateGrid, coordinate);
                    var pattern = new StatePattern(patternElements);
                    stateGrid[coordinate] = GetPartialState(pattern);
                }
            }

            structureLogger.LogStructure("GenFixed", StructureLogLevel.Medium, StructureType.RegionMap, stateGrid);

            foreach (var coordinate in new SolidRectangle(stateGrid.Rectangle))
            {
                int stateId = stateGrid[coordinate];
                int value = _emptyTileId;
                if (stateId != _emptyStateId)
                {
                    int adaptedValue = _possibleStates[stateId].First();
                    value = _templateOriginalValues[adaptedValue];
                }

                structure[coordinate] = value;
            }
        }
    }
}

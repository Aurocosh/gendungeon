﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Tools.Pattern
{
    public class MultiPatternMatcher
    {
        private readonly List<HashSet<int>> _multiPattern;

        public MultiPatternMatcher(IEnumerable<IEnumerable<int>> multiPattern)
        {
            _multiPattern = multiPattern.Select(x => x.ToHashSet()).ToList();
        }

        public bool IsMatching(ValuePattern pattern)
        {
            if (pattern.Count != _multiPattern.Count)
                return false;
            var elements = pattern.Elements;
            int elementCount = pattern.Count;
            for (int i = 0; i < elementCount; i++)
            {
                var value = elements[i];
                var possibleValues = _multiPattern[i];
                if (!possibleValues.Contains(value))
                    return false;
            }
            return true;
        }
    }
}

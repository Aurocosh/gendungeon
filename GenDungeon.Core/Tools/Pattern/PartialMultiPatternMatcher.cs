﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Tools.Pattern
{
    public class PartialMultiPatternMatcher
    {
        private readonly List<HashSet<int>> _multiPattern;

        public PartialMultiPatternMatcher(IEnumerable<IEnumerable<int>> multiPattern)
        {
            _multiPattern = multiPattern.Select(x => x.ToHashSet()).ToList();
        }

        public int CountMatching(ValuePattern pattern)
        {
            if (pattern.Count != _multiPattern.Count)
                return 0;
            var elements = pattern.Elements;
            int matchCount = 0;
            int elementCount = pattern.Count;
            for (int i = 0; i < elementCount; i++)
            {
                var value = elements[i];
                var possibleValues = _multiPattern[i];
                if (possibleValues.Contains(value))
                    matchCount++;
            }
            return matchCount;
        }
    }
}

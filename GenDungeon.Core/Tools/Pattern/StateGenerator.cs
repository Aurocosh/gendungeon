﻿using Combinatorics.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Tools.Pattern
{
    public class StateGenerator
    {
        public List<IList<int>> GenerateStates(IEnumerable<int> values)
        {
            var possibleValues = values.Distinct().ToList();
            int combinationCount = (int)Math.Pow(2, possibleValues.Count);
            var allCombinations = new List<IList<int>>(combinationCount);
            for (int i = 0; i <= possibleValues.Count; i++)
            {
                var combinations = new Combinations<int>(possibleValues, i, GenerateOption.WithoutRepetition);
                allCombinations.AddRange(combinations);
            }
            return allCombinations;
        }
    }
}

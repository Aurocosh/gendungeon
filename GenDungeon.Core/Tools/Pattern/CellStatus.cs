﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Tools.Pattern
{
    public enum CellStatus
    {
        Unplanned,
        Planned,
        Determined,
        Delayed
    }
}

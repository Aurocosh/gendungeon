﻿using System;
using System.Collections;
using System.Collections.Generic;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;

namespace GenDungeon.Core.Tools
{
    public class Room : IComparable<Room>
    {
        public List<Vector2Int> Tiles;
        public List<Vector2Int> EdgeTiles;
        public List<Room> ConnectedRooms;
        public int RoomSize;
        public bool IsAccessibleFromMainRoom;
        public bool IsMainRoom;

        public Room()
        {
        }

        public Room(List<Vector2Int> roomTiles, DungeonStructure map)
        {
            Tiles = roomTiles;
            RoomSize = Tiles.Count;
            ConnectedRooms = new List<Room>();

            EdgeTiles = new List<Vector2Int>();

            foreach (var tile in Tiles)
            {
                foreach (Vector2Int shift in Vector2IntConst.FourDirections)
                {
                    Vector2Int neighbour = tile + shift;
                    if (map.IsItInBounds(neighbour) && map[neighbour] < 0)
                        EdgeTiles.Add(tile);
                }
            }
        }

        public void SetAccessibleFromMainRoom()
        {
            if (!IsAccessibleFromMainRoom)
            {
                IsAccessibleFromMainRoom = true;
                foreach (var connectedRoom in ConnectedRooms)
                {
                    connectedRoom.SetAccessibleFromMainRoom();
                }
            }
        }

        public static void ConnectRooms(Room roomA, Room roomB)
        {
            if (roomA.IsAccessibleFromMainRoom)
            {
                roomB.SetAccessibleFromMainRoom();
            }
            else if (roomB.IsAccessibleFromMainRoom)
            {
                roomA.SetAccessibleFromMainRoom();
            }
            roomA.ConnectedRooms.Add(roomB);
            roomB.ConnectedRooms.Add(roomA);
        }

        public bool IsConnected(Room otherRoom)
        {
            return ConnectedRooms.Contains(otherRoom);
        }

        public int CompareTo(Room otherRoom)
        {
            return otherRoom.RoomSize.CompareTo(RoomSize);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.Data;
using GenDungeon.Core.DataSerialization;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Tools.Connectors;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class RandomLineRoomConnector
    {
        private readonly Random _random;
        private readonly DistributedRandomList<LineConnection> _lineTypeDistribution;

        public RandomLineRoomConnector(Random random, List<LineConnectionData> lineConnections)
        {
            _random = random;
            _lineTypeDistribution = new DistributedRandomList<LineConnection>(random);
            foreach (var connectionData in lineConnections)
                _lineTypeDistribution.Add(connectionData.GetData(), connectionData.Distribution);
        }

        public void Connect(DungeonStructure structure, TileType tileType, RegionData regionData, DungeonStructure regionMap)
        {
            if (regionData.RegionCoordinates.Count < 2)
                return;

            var regionCoordinates = regionData.RegionCoordinates;

            var regionConnectionOrder = regionCoordinates.Keys.ToList();
            regionConnectionOrder.Shuffle(_random);

            var regionConnectionQueue = new Queue<int>(regionConnectionOrder);

            while (regionConnectionQueue.Count > 1)
            {
                int firstRegion = regionConnectionQueue.Dequeue();
                int secondRegion = regionConnectionQueue.Dequeue();

                bool firstExist = regionCoordinates.ContainsKey(firstRegion);
                bool secondExist = regionCoordinates.ContainsKey(secondRegion);

                if (firstExist && secondExist)
                {
                    var firstCoordinate = regionCoordinates[firstRegion].GetRandom(_random);
                    var secondCoordinate = regionCoordinates[secondRegion].GetRandom(_random);

                    var lineConnection = _lineTypeDistribution.GetRandom();
                    var (start, end) = FindClosestConnectionInLine(regionMap, lineConnection.ConnectionType, firstCoordinate, secondCoordinate);
                    int startRegionId = regionMap[start];
                    int endRegionId = regionMap[end];

                    var roundedLine = GetDrawingLine(lineConnection.ConnectionType, start, end, lineConnection.ConnectionThickness);
                    var connectionCoordinates = roundedLine
                        .Where(x =>
                        {
                            if (!regionMap.IsItInBounds(x))
                                return false;
                            int regionId = regionMap[x];
                            return regionId < 0 || regionId == endRegionId;
                        });

                    var endRegionCoordinates = regionCoordinates.GetAndRemove(endRegionId);
                    var changedCoordinates = connectionCoordinates.Concat(endRegionCoordinates).ToList();

                    foreach (var coordinate in changedCoordinates)
                    {
                        regionMap[coordinate] = startRegionId;
                        structure[coordinate] = (int)tileType;
                    }

                    var startRegionCoordinates = regionCoordinates[startRegionId];
                    startRegionCoordinates.AddRange(changedCoordinates);

                    regionConnectionQueue.Enqueue(startRegionId);
                    regionConnectionQueue.Enqueue(secondRegion);
                }
                else
                {
                    if (firstExist)
                        regionConnectionQueue.Enqueue(firstRegion);
                    if (secondExist)
                        regionConnectionQueue.Enqueue(secondRegion);
                }
            }
        }

        private (Vector2Int, Vector2Int) FindClosestConnectionInLine(DungeonStructure regionMap, LineConnectionType connectionType, Vector2Int start, Vector2Int end)
        {
            var searchLine = GetSearchLine(connectionType, start, end);
            int startingRegion = regionMap[start];

            foreach (var coordinate in searchLine)
            {
                int currentRegion = regionMap[coordinate];
                if (currentRegion == startingRegion)
                    start = coordinate;
                else if (currentRegion > 0)
                    return (start, coordinate);
            }
            return (start, end);
        }

        private IEnumerable<Vector2Int> GetSearchLine(LineConnectionType connectionType, Vector2Int start, Vector2Int end)
        {
            switch (connectionType)
            {
                case LineConnectionType.Straight:
                    return new ThinLine(start, end);
                case LineConnectionType.Orthogonal:
                    bool pickFirst = RandomUtility.RollDice(0.5f, _random);
                    var middlePoint = pickFirst ? new Vector2Int(start.X, end.Y) : new Vector2Int(end.X, start.Y);
                    var points = new List<Vector2Int>(3) { start, middlePoint, end };
                    return new PolyLinePoints(points);
                default:
                    return Enumerable.Empty<Vector2Int>();
            }
        }

        private IEnumerable<Vector2Int> GetDrawingLine(LineConnectionType connectionType, Vector2Int start, Vector2Int end, int thickness)
        {
            switch (connectionType)
            {
                case LineConnectionType.Straight:
                    return new ThickLine(start, end, thickness);
                case LineConnectionType.Orthogonal:
                    bool pickFirst = RandomUtility.RollDice(0.5f, _random);
                    var middlePoint = pickFirst ? new Vector2Int(start.X, end.Y) : new Vector2Int(end.X, start.Y);
                    var points = new List<Vector2Int>(3) { start, middlePoint, end };
                    var polyLine = new PolyLinePoints(points);
                    var lines = polyLine.GetLines().Select(x => new ThickLine(x, thickness).Iterator);
                    return lines.Aggregate(Enumerable.Empty<Vector2Int>(), (a, b) => a.Concat(b));
                default:
                    return Enumerable.Empty<Vector2Int>();
            }
        }
    }
}

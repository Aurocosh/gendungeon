﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;

namespace GenDungeon.Core.Tools
{
    public class ClosestRoomConnector
    {
        public void Connect(DungeonStructure structure, TileType tileType, int corridorRadius, RegionData regionData, DungeonStructure regionMap)
        {
            var rooms = ConvertRegionsToRooms(regionData.RegionCoordinates, regionMap);
            ConnectClosestRooms(structure, (int)tileType, corridorRadius, rooms);
        }

        public List<Room> ConvertRegionsToRooms(Dictionary<int, List<Vector2Int>> regionData, DungeonStructure regionMap)
        {
            var rooms = new List<Room>(regionData.Count);
            foreach (var region in regionData)
            {
                var room = new Room(region.Value, regionMap);
                rooms.Add(room);
            }

            if (rooms.Count == 0)
                return rooms;

            rooms.Sort();
            rooms[0].IsMainRoom = true;
            rooms[0].IsAccessibleFromMainRoom = true;

            return rooms;
        }


        public void ConnectClosestRooms(DungeonStructure structure, int tileType, int radius, List<Room> allRooms, bool forceAccessibilityFromMainRoom = false)
        {
            var roomListA = new List<Room>();
            var roomListB = new List<Room>();

            if (forceAccessibilityFromMainRoom)
            {
                foreach (Room room in allRooms)
                {
                    if (room.IsAccessibleFromMainRoom)
                        roomListB.Add(room);
                    else
                        roomListA.Add(room);
                }
            }
            else
            {
                roomListA = allRooms;
                roomListB = allRooms;
            }

            var bestDistance = 0;
            var bestTileA = new Vector2Int();
            var bestTileB = new Vector2Int();
            var bestRoomA = new Room();
            var bestRoomB = new Room();
            var possibleConnectionFound = false;

            foreach (Room roomA in roomListA)
            {
                if (!forceAccessibilityFromMainRoom)
                {
                    possibleConnectionFound = false;
                    if (roomA.ConnectedRooms.Count > 0)
                        continue;
                }

                foreach (Room roomB in roomListB)
                {
                    if (roomA == roomB || roomA.IsConnected(roomB))
                        continue;

                    foreach (Vector2Int tA in roomA.EdgeTiles)
                    {
                        foreach (Vector2Int tB in roomB.EdgeTiles)
                        {
                            Vector2Int tileA = tA;
                            Vector2Int tileB = tB;

                            int xDif = tileA.X - tileB.X;
                            int yDif = tileA.Y - tileB.Y;

                            int distanceBetweenRooms = (xDif * xDif) + (yDif * yDif);
                            if (distanceBetweenRooms >= bestDistance && possibleConnectionFound)
                                continue;

                            bestDistance = distanceBetweenRooms;
                            possibleConnectionFound = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }
                    }
                }
                if (possibleConnectionFound && !forceAccessibilityFromMainRoom)
                    CreatePassage(structure, tileType, radius, bestRoomA, bestRoomB, bestTileA, bestTileB);
            }

            if (possibleConnectionFound && forceAccessibilityFromMainRoom)
            {
                CreatePassage(structure, tileType, radius, bestRoomA, bestRoomB, bestTileA, bestTileB);
                ConnectClosestRooms(structure, tileType, radius, allRooms, true);
            }

            if (!forceAccessibilityFromMainRoom)
                ConnectClosestRooms(structure, tileType, radius, allRooms, true);
        }

        private static void CreatePassage(DungeonStructure structure, int tileType, int radius, Room roomA, Room roomB, Vector2Int tileA, Vector2Int tileB)
        {
            Room.ConnectRooms(roomA, roomB);
            var roundedLine = new RoundedLine(tileA, tileB, radius);
            DungeonStructureDrawing.DrawPointCloud(structure, roundedLine, tileType);
        }
    }
}

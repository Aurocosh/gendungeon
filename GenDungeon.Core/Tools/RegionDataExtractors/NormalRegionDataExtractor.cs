﻿using System.Collections.Generic;
using System.Drawing;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;

namespace GenDungeon.Core.Tools
{
    public class NormalRegionDataExtractor
    {
        public RegionData Extract(DungeonStructure structure)
        {
            var regionCoordinates = new Dictionary<int, List<Vector2Int>>();
            for (int x = 0; x < structure.Width; x++)
            {
                for (int y = 0; y < structure.Height; y++)
                {
                    int regionId = structure[x, y];
                    if (regionId > 0)
                    {
                        var coordinates = regionCoordinates.ComputeIfAbsent(regionId, () => new List<Vector2Int>());
                        coordinates.Add(new Vector2Int(x, y));
                    }
                }
            }
            return new RegionData(regionCoordinates);
        }
    }
}

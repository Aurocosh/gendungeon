﻿using System.Collections.Generic;
using System.Drawing;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;

namespace GenDungeon.Core.Tools
{
    public class SingleTypeRegionMapExtractor
    {
        public DungeonStructure Extract(DungeonStructure structure, TileType tileType)
        {
            int type = (int)tileType;
            var regionMap = new DungeonStructure(structure.Width, structure.Height);
            var rectangle = structure.Rectangle;

            foreach (var coordinate in new SolidRectangle(rectangle))
            {
                var tile = structure[coordinate];
                regionMap[coordinate] = tile == type ? 0 : -1;
            }

            int nextRegionId = 1;
            foreach (var coordinate in new SolidRectangle(rectangle))
            {
                var tile = regionMap[coordinate];
                if (tile == 0)
                {
                    int regionId = nextRegionId++;
                    FloodFill.FloodFillValue(regionMap, coordinate, regionId, id => id == 0);
                }
            }

            return regionMap;
        }
    }
}

﻿/*
The MIT License(MIT)
Copyright(c) mxgmn 2016.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
The software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.
*/

using GenDungeon.Core.Dungeon;
using System;

namespace WaveFunction
{
    internal abstract class Model
    {
        protected bool[][] wave;

        protected int[][][] propagator;
        protected int[] observed;

        private int[][][] _compatible;
        private (int, int)[] _stack;
        private int _stacksize;

        protected Random random;

        protected bool periodic;
        protected int resultWidth;
        protected int resultHeight;
        protected int actualPatternCount;

        protected double[] weights;
        double[] weightLogWeights;

        int[] sumsOfOnes;
        double sumOfWeights, sumOfWeightLogWeights, startingEntropy;
        double[] sumsOfWeights, sumsOfWeightLogWeights, entropies;

        protected Model(int width, int height, Random random)
        {
            this.random = random;
            resultWidth = width;
            resultHeight = height;
        }

        void Init()
        {
            wave = new bool[resultWidth * resultHeight][];
            _compatible = new int[wave.Length][][];
            for (int i = 0; i < wave.Length; i++)
            {
                wave[i] = new bool[actualPatternCount];
                _compatible[i] = new int[actualPatternCount][];
                for (int t = 0; t < actualPatternCount; t++)
                    _compatible[i][t] = new int[4];
            }

            weightLogWeights = new double[actualPatternCount];
            sumOfWeights = 0;
            sumOfWeightLogWeights = 0;

            for (int t = 0; t < actualPatternCount; t++)
            {
                weightLogWeights[t] = weights[t] * Math.Log(weights[t]);
                sumOfWeights += weights[t];
                sumOfWeightLogWeights += weightLogWeights[t];
            }

            startingEntropy = Math.Log(sumOfWeights) - (sumOfWeightLogWeights / sumOfWeights);

            sumsOfOnes = new int[resultWidth * resultHeight];
            sumsOfWeights = new double[resultWidth * resultHeight];
            sumsOfWeightLogWeights = new double[resultWidth * resultHeight];
            entropies = new double[resultWidth * resultHeight];

            _stack = new (int, int)[wave.Length * actualPatternCount];
            _stacksize = 0;
        }

        bool? Observe()
        {
            double min = 1E+3;
            int argmin = -1;

            for (int i = 0; i < wave.Length; i++)
            {
                if (OnBoundary(i % resultWidth, i / resultWidth))
                    continue;

                int amount = sumsOfOnes[i];
                if (amount == 0)
                    return false;

                double entropy = entropies[i];
                if (amount > 1 && entropy <= min)
                {
                    double noise = 1E-6 * random.NextDouble();
                    if (entropy + noise < min)
                    {
                        min = entropy + noise;
                        argmin = i;
                    }
                }
            }

            if (argmin == -1)
            {
                observed = new int[resultWidth * resultHeight];
                for (int i = 0; i < wave.Length; i++)
                    for (int t = 0; t < actualPatternCount; t++)
                        if (wave[i][t]) { observed[i] = t; break; }
                return true;
            }

            double[] distribution = new double[actualPatternCount];
            for (int t = 0; t < actualPatternCount; t++) distribution[t] = wave[argmin][t] ? weights[t] : 0;
            int r = distribution.Random(random.NextDouble());

            bool[] w = wave[argmin];
            for (int t = 0; t < actualPatternCount; t++) if (w[t] != (t == r)) Ban(argmin, t);

            return null;
        }

        protected void Propagate()
        {
            while (_stacksize > 0)
            {
                var e1 = _stack[_stacksize - 1];
                _stacksize--;

                int i1 = e1.Item1;
                int x1 = i1 % resultWidth, y1 = i1 / resultWidth;

                for (int d = 0; d < 4; d++)
                {
                    int dx = DX[d], dy = DY[d];
                    int x2 = x1 + dx, y2 = y1 + dy;
                    if (OnBoundary(x2, y2)) continue;

                    if (x2 < 0) x2 += resultWidth;
                    else if (x2 >= resultWidth) x2 -= resultWidth;
                    if (y2 < 0) y2 += resultHeight;
                    else if (y2 >= resultHeight) y2 -= resultHeight;

                    int i2 = x2 + (y2 * resultWidth);
                    int[] p = propagator[d][e1.Item2];
                    int[][] compat = _compatible[i2];

                    for (int l = 0; l < p.Length; l++)
                    {
                        int t2 = p[l];
                        int[] comp = compat[t2];

                        comp[d]--;
                        if (comp[d] == 0) Ban(i2, t2);
                    }
                }
            }
        }

        public bool Run(int limit)
        {
            if (wave == null)
                Init();

            Clear();

            for (int l = 0; l < limit || limit == 0; l++)
            {
                bool? result = Observe();
                if (result != null)
                    return (bool)result;
                Propagate();
            }

            return true;
        }

        protected void Ban(int i, int t)
        {
            wave[i][t] = false;

            int[] comp = _compatible[i][t];
            for (int d = 0; d < 4; d++) comp[d] = 0;
            _stack[_stacksize] = (i, t);
            _stacksize++;

            sumsOfOnes[i]--;
            sumsOfWeights[i] -= weights[t];
            sumsOfWeightLogWeights[i] -= weightLogWeights[t];

            double sum = sumsOfWeights[i];
            entropies[i] = Math.Log(sum) - (sumsOfWeightLogWeights[i] / sum);
        }

        protected virtual void Clear()
        {
            for (int i = 0; i < wave.Length; i++)
            {
                for (int t = 0; t < actualPatternCount; t++)
                {
                    wave[i][t] = true;
                    for (int d = 0; d < 4; d++) _compatible[i][t][d] = propagator[_opposite[d]][t].Length;
                }

                sumsOfOnes[i] = weights.Length;
                sumsOfWeights[i] = sumOfWeights;
                sumsOfWeightLogWeights[i] = sumOfWeightLogWeights;
                entropies[i] = startingEntropy;
            }
        }

        protected abstract bool OnBoundary(int x, int y);

        public abstract void GetResult(DungeonStructure structure);

        protected static readonly int[] DX = { -1, 0, 1, 0 };
        protected static readonly int[] DY = { 0, 1, 0, -1 };
        private static readonly int[] _opposite = { 2, 3, 0, 1 };
    }
}
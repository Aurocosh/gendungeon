﻿/*
The MIT License(MIT)
Copyright(c) mxgmn 2016.
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
The software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.
*/

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Primitives;
using System.Linq;

namespace WaveFunction
{
    internal sealed class OverlappingModel : Model
    {
        private readonly int _groundLevel;
        private readonly int _patternSize;
        private readonly byte[][] _patterns;
        private readonly List<int> _originalTemplateValues;

        public OverlappingModel(Random random, DungeonStructure template, int patternSize, int width, int height, bool periodicInput, bool periodicOutput, int symmetry, int ground)
            : base(width, height, random)
        {
            _patternSize = patternSize;
            periodic = periodicOutput;

            int templateWidth = template.Width;
            int templateHeight = template.Height;

            var templateRect = new SolidRectangle(template.Rectangle);
            _originalTemplateValues = templateRect
                .Select(x => template[x])
                .Distinct()
                .ToList();

            var templateAdaptationMap = _originalTemplateValues
                .Select((v, i) => (v, i))
                .ToDictionary(x => x.v, x => x.i);

            byte[,] adaptedTemplate = new byte[templateWidth, templateHeight];
            foreach (var coordinate in templateRect)
            {
                var id = template[coordinate];
                var adaptedTemplateId = templateAdaptationMap[id];
                adaptedTemplate[coordinate.X, coordinate.Y] = (byte)adaptedTemplateId;
            }

            int originalValueCount = _originalTemplateValues.Count;
            long possiblePatternCount = originalValueCount.ToPower(patternSize * patternSize);

            byte[] generatePattern(Func<int, int, byte> f)
            {
                byte[] result = new byte[patternSize * patternSize];
                for (int y = 0; y < patternSize; y++)
                {
                    for (int x = 0; x < patternSize; x++)
                    {
                        result[x + (y * patternSize)] = f(x, y);
                    }
                }

                return result;
            }

            byte[] patternFromSample(int x, int y) => generatePattern((dx, dy) => adaptedTemplate[(x + dx) % templateWidth, (y + dy) % templateHeight]);
            byte[] rotate(byte[] pattern) => generatePattern((x, y) => pattern[patternSize - 1 - y + (x * patternSize)]);
            byte[] reflect(byte[] pattern) => generatePattern((x, y) => pattern[patternSize - 1 - x + (y * patternSize)]);

            long indexFromPattern(byte[] pattern)
            {
                long result = 0, power = 1;
                for (int i = 0; i < pattern.Length; i++)
                {
                    result += pattern[pattern.Length - 1 - i] * power;
                    power *= originalValueCount;
                }
                return result;
            }

            byte[] patternFromIndex(long index)
            {
                long residue = index;
                long power = possiblePatternCount;
                byte[] result = new byte[patternSize * patternSize];

                for (int i = 0; i < result.Length; i++)
                {
                    power /= originalValueCount;
                    int count = 0;

                    while (residue >= power)
                    {
                        residue -= power;
                        count++;
                    }

                    result[i] = (byte)count;
                }

                return result;
            }

            var ordering = new List<long>();
            var weights = new Dictionary<long, int>();

            int maxX = periodicInput ? templateWidth : templateWidth - patternSize + 1;
            int maxY = periodicInput ? templateHeight : templateHeight - patternSize + 1;
            for (int y = 0; y < maxY; y++)
            {
                for (int x = 0; x < maxX; x++)
                {
                    byte[][] patternVariations = new byte[8][];

                    patternVariations[0] = patternFromSample(x, y);
                    patternVariations[1] = reflect(patternVariations[0]);
                    patternVariations[2] = rotate(patternVariations[0]);
                    patternVariations[3] = reflect(patternVariations[2]);
                    patternVariations[4] = rotate(patternVariations[2]);
                    patternVariations[5] = reflect(patternVariations[4]);
                    patternVariations[6] = rotate(patternVariations[4]);
                    patternVariations[7] = reflect(patternVariations[6]);

                    for (int k = 0; k < symmetry; k++)
                    {
                        long patternIndex = indexFromPattern(patternVariations[k]);
                        if (weights.TryGetValue(patternIndex, out int weight))
                        {
                            weights[patternIndex] = weight + 1;
                        }
                        else
                        {
                            weights.Add(patternIndex, 1);
                            ordering.Add(patternIndex);
                        }
                    }
                }
            }

            actualPatternCount = weights.Count;
            _groundLevel = (ground + actualPatternCount) % actualPatternCount;
            _patterns = new byte[actualPatternCount][];
            base.weights = new double[actualPatternCount];

            int counter = 0;
            foreach (long patternIndex in ordering)
            {
                _patterns[counter] = patternFromIndex(patternIndex);
                base.weights[counter] = weights[patternIndex];
                counter++;
            }

            bool agrees(byte[] firstPattern, byte[] secondPattern, int dx, int dy)
            {
                int xmin = dx < 0 ? 0 : dx;
                int xmax = dx < 0 ? dx + patternSize : patternSize;
                int ymin = dy < 0 ? 0 : dy;
                int ymax = dy < 0 ? dy + patternSize : patternSize;
                for (int y = ymin; y < ymax; y++)
                {
                    for (int x = xmin; x < xmax; x++)
                    {
                        if (firstPattern[x + (patternSize * y)] != secondPattern[x - dx + (patternSize * (y - dy))])
                            return false;
                    }
                }

                return true;
            }

            propagator = new int[4][][];
            for (int directionIndex = 0; directionIndex < 4; directionIndex++)
            {
                propagator[directionIndex] = new int[actualPatternCount][];
                for (int fromPatternIndex = 0; fromPatternIndex < actualPatternCount; fromPatternIndex++)
                {
                    var list = new List<int>();
                    for (int toPatternIndex = 0; toPatternIndex < actualPatternCount; toPatternIndex++)
                    {
                        if (agrees(_patterns[fromPatternIndex], _patterns[toPatternIndex], DX[directionIndex], DY[directionIndex]))
                            list.Add(toPatternIndex);
                    }

                    propagator[directionIndex][fromPatternIndex] = list.ToArray();
                }
            }
        }

        protected override bool OnBoundary(int x, int y) => !periodic && (x + _patternSize > resultWidth || y + _patternSize > resultHeight || x < 0 || y < 0);

        public override void GetResult(DungeonStructure structure)
        {
            if (structure.Width != resultWidth || structure.Height != resultHeight)
                return;

            if (observed != null)
            {
                for (int y = 0; y < resultHeight; y++)
                {
                    int dy = y < resultHeight - _patternSize + 1 ? 0 : _patternSize - 1;
                    for (int x = 0; x < resultWidth; x++)
                    {
                        int dx = x < resultWidth - _patternSize + 1 ? 0 : _patternSize - 1;
                        structure[x, y] = _originalTemplateValues[_patterns[observed[x - dx + ((y - dy) * resultWidth)]][dx + (dy * _patternSize)]];
                    }
                }
            }
        }

        //public override Bitmap Graphics()
        //{
        //    Bitmap result = new Bitmap(FMX, FMY);
        //    int[] bitmapData = new int[result.Height * result.Width];

        //    if (observed != null)
        //    {
        //        for (int y = 0; y < FMY; y++)
        //        {
        //            int dy = y < FMY - N + 1 ? 0 : N - 1;
        //            for (int x = 0; x < FMX; x++)
        //            {
        //                int dx = x < FMX - N + 1 ? 0 : N - 1;
        //                Color c = colors[patterns[observed[x - dx + (y - dy) * FMX]][dx + dy * N]];
        //                bitmapData[x + y * FMX] = unchecked((int)0xff000000 | (c.R << 16) | (c.G << 8) | c.B);
        //            }
        //        }
        //    }
        //    else
        //    {
        //        for (int i = 0; i < wave.Length; i++)
        //        {
        //            int contributors = 0, r = 0, g = 0, b = 0;
        //            int x = i % FMX, y = i / FMX;

        //            for (int dy = 0; dy < N; dy++) for (int dx = 0; dx < N; dx++)
        //                {
        //                    int sx = x - dx;
        //                    if (sx < 0) sx += FMX;

        //                    int sy = y - dy;
        //                    if (sy < 0) sy += FMY;

        //                    int s = sx + sy * FMX;
        //                    if (OnBoundary(sx, sy)) continue;
        //                    for (int t = 0; t < T; t++) if (wave[s][t])
        //                        {
        //                            contributors++;
        //                            Color color = colors[patterns[t][dx + dy * N]];
        //                            r += color.R;
        //                            g += color.G;
        //                            b += color.B;
        //                        }
        //                }

        //            bitmapData[i] = unchecked((int)0xff000000 | ((r / contributors) << 16) | ((g / contributors) << 8) | b / contributors);
        //        }
        //    }

        //    var bits = result.LockBits(new Rectangle(0, 0, result.Width, result.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
        //    System.Runtime.InteropServices.Marshal.Copy(bitmapData, 0, bits.Scan0, bitmapData.Length);
        //    result.UnlockBits(bits);

        //    return result;
        //}

        protected override void Clear()
        {
            base.Clear();

            if (_groundLevel != 0)
            {
                for (int x = 0; x < resultWidth; x++)
                {
                    for (int t = 0; t < actualPatternCount; t++)
                    {
                        if (t != _groundLevel)
                        {
                            Ban(x + ((resultHeight - 1) * resultWidth), t);
                        }
                    }

                    for (int y = 0; y < resultHeight - 1; y++)
                        Ban(x + (y * resultWidth), _groundLevel);
                }

                Propagate();
            }
        }
    }
}
﻿using GenDungeon.Core.Dungeon;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaveFunction;

namespace GenDungeon.Core.Tools.WaveFunction
{
    public class WaveFunctionOverlappingFiller
    {
        private readonly Random _random;

        public WaveFunctionOverlappingFiller(Random random)
        {
            _random = random;
        }

        public void Fill(DungeonStructure structure, DungeonStructure template, int N, int symmetry, int ground, bool periodicInput, bool periodic, int limit)
        {
            Fill(structure, template, N, symmetry, ground, periodicInput, periodic, limit, new Rectangle(0, 0, structure.Width, structure.Height));
        }

        public void Fill(DungeonStructure structure, DungeonStructure template, int N, int symmetry, int ground, bool periodicInput, bool periodic, int limit, Rectangle rectangle)
        {
            var model = new OverlappingModel(_random, template, N, structure.Width, structure.Height, periodicInput, periodic, symmetry, ground);
            bool finished = model.Run(limit);
            model.GetResult(structure);
        }
    }
}

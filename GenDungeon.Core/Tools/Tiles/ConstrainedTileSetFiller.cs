﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
////////using System.Threading.Tasks;
using DotNet.Collections.Generic;
using GenDungeon.Core.Data.Tiles;
using GenDungeon.Core.DataTypes;
using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Grids;
using GenDungeon.Core.Inputs;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Lib.Extensions;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.TileSets;
using GenDungeon.Core.TileSets.Tiles;
using GenDungeon.Core.Tools.Tiles;
using GenDungeon.Core.Utility;
using Newtonsoft.Json;
using Priority_Queue;

namespace GenDungeon.Core.Tools
{
    public class ConstrainedTileSetFiller
    {
        private readonly Random _random;
        private readonly TileSet _tileSet;
        private readonly MultiMapList<ConnectionState, Tile> _configurations;

        public ConstrainedTileSetFiller(Random random, TileSet tileSet)
        {
            _random = random;
            _tileSet = tileSet;

            _configurations = new MultiMapList<ConnectionState, Tile>();
            foreach (var tile in tileSet.Tiles)
            {
                foreach (var connnectionState in tile.ValidConnectionStates)
                {
                    _configurations.TryToAddMapping(connnectionState, tile);
                }
            }
        }

        public void Fill(DungeonStructure structure)
        {
            Fill(structure, new SolidRectangle(structure.Rectangle));
        }

        public void Fill(DungeonStructure structure, ICoordinateShape shape)
        {
            var tiles = new Grid<HashSet<int>>(structure.Width, structure.Height);
            var possibleConnections = new Grid<List<List<int>>>(structure.Width, structure.Height);

            var priorityQueue = new SimplePriorityQueue<Vector2Int, int>();
            foreach (var coordinate in shape)
            {
                var possibleTileIds = GetPossibleTiles(null, shape, coordinate);
                tiles[coordinate] = possibleTileIds;
                priorityQueue.Enqueue(coordinate, possibleTileIds.Count);
            }

            int tilesProcessed = 0;

            while (priorityQueue.Count > 0)
            {
                var coordinate = priorityQueue.Dequeue();
                var possibleTileIds = tiles[coordinate];
                if (possibleTileIds.Count == 1)
                    continue;

                tilesProcessed++;

                var selectedTileId = possibleTileIds.ToList().GetRandom(_random);
                tiles[coordinate] = new HashSet<int> { selectedTileId };

                var neighbours = Vector2IntConst.FourDirections.Select(x => x + coordinate);
                var propagationFront = new Queue<Vector2Int>(neighbours);
                while (propagationFront.Count > 0)
                {
                    var nextPropagated = propagationFront.Dequeue();
                    if (shape.IsInBounds(nextPropagated))
                    {
                        var currentTileIds = tiles[nextPropagated];
                        if (currentTileIds.Count == 1)
                            continue;

                        var newTileIds = GetPossibleTiles(tiles, shape, nextPropagated);
                        if (newTileIds.Count < currentTileIds.Count)
                        {
                            tiles[nextPropagated] = newTileIds;
                            priorityQueue.Enqueue(nextPropagated, newTileIds.Count);

                            var nextNeighbours = Vector2IntConst.FourDirections.Select(x => x + nextPropagated);
                            foreach (var nextCoordinate in nextNeighbours)
                                propagationFront.Enqueue(nextCoordinate);
                        }
                    }
                }
            }

            foreach (var coordinate in new SolidRectangle(tiles.Rectangle))
                structure[coordinate] = tiles[coordinate].FirstOrDefault();
        }

        private HashSet<int> GetPossibleTiles(Grid<HashSet<int>> tiles, ICoordinateShape shape, Vector2Int coordinate)
        {
            var possibleTiles = new HashSet<int>();
            foreach (var state in GetPossibleConnectionStates(tiles, shape, coordinate))
            {
                if (_configurations.TryGetValue(state, out var addedTiles))
                    possibleTiles.UnionWith(addedTiles.Select(x => x.Id));
            }
            return possibleTiles;
        }

        private IEnumerable<ConnectionState> GetPossibleConnectionStates(Grid<HashSet<int>> tileIdGrid, ICoordinateShape shape, Vector2Int coordinate)
        {
            var upConnections = GetConnectionsInDirection(tileIdGrid, shape, coordinate, ConnectionDirection.Up);
            var rightConnections = GetConnectionsInDirection(tileIdGrid, shape, coordinate, ConnectionDirection.Right);
            var downConnections = GetConnectionsInDirection(tileIdGrid, shape, coordinate, ConnectionDirection.Down);
            var leftConnections = GetConnectionsInDirection(tileIdGrid, shape, coordinate, ConnectionDirection.Left);

            return
                from upId in upConnections
                from rightId in rightConnections
                from downId in downConnections
                from leftId in leftConnections
                select new ConnectionState(upId, rightId, downId, leftId);
        }

        private IEnumerable<int> GetConnectionsInDirection(Grid<HashSet<int>> tileIdGrid, ICoordinateShape shape, Vector2Int origin, ConnectionDirection direction)
        {
            var shift = ConnectionDirectionUtility.GetDirectionShift(direction);
            var coordinate = origin + shift;
            if (!shape.IsInBounds(coordinate))
                return _tileSet.BorderConnectionId.Yield();

            var tileIds = tileIdGrid?[coordinate];
            if (tileIds is null)
                return (-1).Yield();

            var opposite = ConnectionDirectionUtility.GetOpposite(direction);
            return tileIds.SelectMany(x => _tileSet.GetTile(x).GetConnections(opposite));
        }
    }
}

﻿using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.PerlinNoize
{
    public class PerlinNoizeGenerator
    {
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public float Scale { get; set; }

        public PerlinNoizeGenerator()
        {
            OffsetX = 0;
            OffsetY = 0;
            Scale = 1;
        }

        public PerlinNoizeGenerator(int offsetX, int offsetY, float scale)
        {
            OffsetX = offsetX;
            OffsetY = offsetY;
            Scale = scale;
        }

        public float Noise(float x) => Perlin.Noise((x + OffsetX) * Scale);
        public float Noise(float x, float y) => Perlin.Noise((x + OffsetX) * Scale, (y + OffsetY) * Scale);

        public float Noise(Vector2 coordinate) => Noise(coordinate.X, coordinate.Y);
        public float Noise(Vector2Int coordinate) => Noise(coordinate.X, coordinate.Y);
    }
}

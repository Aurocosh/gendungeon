﻿using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.ShapeProvider.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ShapeProvider
{
    public class CircleShapeProvider : IShapeProvider
    {
        public int Radius { get; set; }

        public IEnumerable<Vector2Int> GetShape()
        {
            return new SolidCircle(Vector2Int.Zero, Radius);
        }
    }
}

﻿using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ShapeProvider.Base
{
    public interface IShapeProvider
    {
        IEnumerable<Vector2Int> GetShape();
    }
}

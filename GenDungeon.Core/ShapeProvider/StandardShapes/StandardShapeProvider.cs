﻿using GenDungeon.Core.Lib;
using GenDungeon.Core.ShapeProvider.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ShapeProvider
{
    public class StandardShapeProvider : IShapeProvider
    {
        public StandardShape Shape { get; set; }

        public IEnumerable<Vector2Int> GetShape()
        {
            switch (Shape)
            {
                case StandardShape.FourNeighbours:
                    return Vector2IntConst.FourDirections;
                case StandardShape.EightNeighbours:
                    return Vector2IntConst.EightDirections;
                case StandardShape.CornerNeighbours:
                    return new Vector2Int[] { Vector2IntConst.PosXPosY, Vector2IntConst.PosXNegY, Vector2IntConst.NegXNegY, Vector2IntConst.NegXPosY };
            }
            throw new ArgumentException("Invalid shape argument");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ShapeProvider
{
    public enum StandardShape
    {
        FourNeighbours,
        EightNeighbours,
        CornerNeighbours,
    }
}

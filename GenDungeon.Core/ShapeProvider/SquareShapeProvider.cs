﻿using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using GenDungeon.Core.ShapeProvider.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ShapeProvider
{
    public class SquareShapeProvider : IShapeProvider
    {
        public int Radius { get; set; }

        public IEnumerable<Vector2Int> GetShape()
        {
            var originShift = 1 - Radius;
            var sideLength = (2 * Radius) - 1;
            return new SolidRectangle(originShift, originShift, sideLength, sideLength);
        }
    }
}

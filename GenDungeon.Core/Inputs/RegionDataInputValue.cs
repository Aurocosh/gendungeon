﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.RandomProvider;
using GenDungeon.Core.RandomProvider.Base;
using System;

namespace GenDungeon.Core.Inputs
{
    public class RegionDataInputValue : InputValue<IRandomProvider>
    {
        public RegionDataInputValue(string name) : base(name, ConverterInputType.RegionData)
        {
        }

        public static RegionData Cast(object value)
        {
            if (value is RegionData regionData)
                return regionData;
            return new RegionData();
        }
    }
}

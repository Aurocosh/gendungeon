﻿using GenDungeon.Core.Inputs.Base;
using GenDungeon.Core.RandomProvider;
using GenDungeon.Core.RandomProvider.Base;
using System;

namespace GenDungeon.Core.Inputs
{
    public class RandomProviderInputValue : InputValue<IRandomProvider>
    {
        public RandomProviderInputValue(string name) : base(name, ConverterInputType.RandomProvider)
        {
        }

        public static Random Cast(object value)
        {
            if (value is Random structure)
                return structure;
            return new Random();
        }
    }
}

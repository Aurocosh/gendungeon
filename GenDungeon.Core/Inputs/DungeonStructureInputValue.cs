﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Inputs.Base;

namespace GenDungeon.Core.Inputs
{
    public class DungeonStructureInputValue : InputValue<DungeonStructure>
    {
        public DungeonStructureInputValue(string name) : base(name, ConverterInputType.DungeonStructure)
        {
        }

        public static DungeonStructure Cast(object value)
        {
            if (value is DungeonStructure structure)
                return structure;
            return new DungeonStructure();
        }
    }
}

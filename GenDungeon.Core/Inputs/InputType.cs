﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Inputs
{
    public enum ConverterInputType
    {
        DungeonStructure,
        RandomProvider,
        RegionData,
        ColorSet
    }
}

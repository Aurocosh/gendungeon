﻿namespace GenDungeon.Core.Inputs.Base
{
    public interface IInputValue
    {
        string Name { get; }
        ConverterInputType InputType { get; }
    }
}

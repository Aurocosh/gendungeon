﻿namespace GenDungeon.Core.Inputs.Base
{
    public abstract class InputValue<T> : IInputValue where T : class
    {
        public string Name { get; }
        public ConverterInputType InputType { get; }

        protected InputValue(string name, ConverterInputType inputType)
        {
            Name = name;
            InputType = inputType;
        }
    }
}

﻿using GenDungeon.Core.Tools.Connectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Data
{
    public class LineConnection
    {
        public int ConnectionThickness { get; }
        public LineConnectionType ConnectionType { get; }

        public LineConnection(int connectionThickness, LineConnectionType connectionType)
        {
            ConnectionThickness = connectionThickness;
            ConnectionType = connectionType;
        }
    }
}

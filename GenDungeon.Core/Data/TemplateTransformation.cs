﻿using GenDungeon.Core.Dungeon.Converter;
using GenDungeon.Core.InputData;
using GenDungeon.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Data
{
    public class TemplateTransformation
    {
        public StructureMirror Mirror { get; }
        public List<StructureRotation> Rotations { get; }

        public TemplateTransformation(StructureMirror mirror, IEnumerable<StructureRotation> rotations)
        {
            Mirror = mirror;
            Rotations = rotations.ToList();
        }
    }
}

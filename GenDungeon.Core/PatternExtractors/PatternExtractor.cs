﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using GenDungeon.Core.PatternExtractors.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.PatternExtractors
{
    public class NormalPatternExtractor : IPatternExtractor
    {
        private readonly int _outOfBoundsValue;
        private readonly List<Vector2Int> _coordinateShifts;

        public NormalPatternExtractor(int outOfBoundsValue, IEnumerable<Vector2Int> coordinateShifts)
        {
            _outOfBoundsValue = outOfBoundsValue;
            _coordinateShifts = coordinateShifts.ToList();
        }

        public IEnumerable<int> ExtractPattern(IReadOnlyDungeonStructure source, Vector2Int origin)
        {
            foreach (var shift in _coordinateShifts)
            {
                var coordinate = origin + shift;
                if (source.IsItInBounds(coordinate))
                    yield return source[coordinate];
                else
                    yield return _outOfBoundsValue;
            }
        }
    }
}

﻿using GenDungeon.Core.Lib;
using GenDungeon.Core.PatternExtractors.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.PatternExtractors
{
    public static class PatternExtractorFactory
    {
        public static IPatternExtractor GetPatternExtractor(CoordinateWrapping wrapping, int outOfBoundsValue, IEnumerable<Vector2Int> coordinateShifts)
        {
            switch (wrapping)
            {
                case CoordinateWrapping.NoWrap:
                    return new NormalPatternExtractor(outOfBoundsValue, coordinateShifts);
                case CoordinateWrapping.WrapX:
                    return new XWrappingPatternExtractor(outOfBoundsValue, coordinateShifts);
                case CoordinateWrapping.WrapY:
                    return new YWrappingPatternExtractor(outOfBoundsValue, coordinateShifts);
                case CoordinateWrapping.WrapXY:
                    return new XYWrappingPatternExtractor(coordinateShifts);
            }
            throw new ArgumentException("Unknown wrap type");
        }
    }
}

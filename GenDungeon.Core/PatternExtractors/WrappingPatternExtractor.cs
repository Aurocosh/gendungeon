﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using GenDungeon.Core.PatternExtractors.Base;
using GenDungeon.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.PatternExtractors
{
    public class WrappingPatternExtractor : IPatternExtractor
    {
        private readonly bool _wrapX;
        private readonly bool _wrapY;
        private readonly int _outOfBoundsValue;
        private readonly List<Vector2Int> _coordinateShifts;

        public WrappingPatternExtractor(CoordinateWrapping wrapping, int outOfBoundsValue, IEnumerable<Vector2Int> coordinateShifts)
        {
            _wrapX = wrapping == CoordinateWrapping.WrapX || wrapping == CoordinateWrapping.WrapXY;
            _wrapY = wrapping == CoordinateWrapping.WrapY || wrapping == CoordinateWrapping.WrapXY;

            _outOfBoundsValue = outOfBoundsValue;
            _coordinateShifts = coordinateShifts.ToList();
        }

        public IEnumerable<int> ExtractPattern(IReadOnlyDungeonStructure source, Vector2Int origin)
        {
            foreach (var shift in _coordinateShifts)
            {
                var coordinate = origin + shift;
                var x = _wrapX ? CoordinateUtilis.WrapCoordinate(coordinate.X, source.Width) : coordinate.X;
                var y = _wrapY ? CoordinateUtilis.WrapCoordinate(coordinate.Y, source.Height) : coordinate.Y;
                if (source.IsItInBounds(x, y))
                    yield return source[x, y];
                else
                    yield return _outOfBoundsValue;
            }
        }
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using GenDungeon.Core.PatternExtractors.Base;
using GenDungeon.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.PatternExtractors
{
    public class XYWrappingPatternExtractor : IPatternExtractor
    {
        private readonly List<Vector2Int> _coordinateShifts;

        public XYWrappingPatternExtractor(IEnumerable<Vector2Int> coordinateShifts)
        {
            _coordinateShifts = coordinateShifts.ToList();
        }

        public IEnumerable<int> ExtractPattern(IReadOnlyDungeonStructure source, Vector2Int origin)
        {
            foreach (var shift in _coordinateShifts)
            {
                var coordinate = origin + shift;
                var x = CoordinateUtilis.WrapCoordinate(coordinate.X, source.Width);
                var y = CoordinateUtilis.WrapCoordinate(coordinate.Y, source.Height);
                yield return source[x, y];
            }
        }
    }
}

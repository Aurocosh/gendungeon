﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.PatternExtractors.Base
{
    public interface IPatternExtractor
    {
        IEnumerable<int> ExtractPattern(IReadOnlyDungeonStructure source, Vector2Int origin);
    }
}

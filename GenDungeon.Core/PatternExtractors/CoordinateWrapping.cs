﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.PatternExtractors
{
    public enum CoordinateWrapping
    {
        NoWrap,
        WrapX,
        WrapY,
        WrapXY
    }
}

﻿using GenDungeon.Core.Dungeon;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public static class FloodFill
    {
        public static void FloodFillValue(DungeonStructure structure, Vector2Int start, int floodValue, Func<int, bool> isSameType)
        {
            var observed = new HashSet<Vector2Int>();
            var front = new Queue<Vector2Int>();

            front.Enqueue(start);
            while (front.Count > 0)
            {
                var nextPoint = front.Dequeue();
                var nextValue = structure[nextPoint];
                if (isSameType(nextValue))
                {
                    structure[nextPoint] = floodValue;
                    var nextFront = Vector2IntConst.FourDirections
                        .Select(x => nextPoint + x)
                        .Where(x => structure.IsItInBounds(x) && !observed.Contains(x));

                    foreach (var vector in nextFront)
                        front.Enqueue(vector);
                }
            }
        }
    }
}

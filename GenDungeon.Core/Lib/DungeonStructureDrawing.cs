﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public static class DungeonStructureDrawing
    {
        public static void DrawPointCloud(DungeonStructure structure, IEnumerable<Vector2Int> coordinates, int tileType)
        {
            foreach (var coordinate in coordinates)
                structure[coordinate] = tileType;
        }

        public static void DrawPointCloudSafe(DungeonStructure structure, IEnumerable<Vector2Int> coordinates, int tileType)
        {
            foreach (var coordinate in coordinates)
            {
                if (structure.IsItInBounds(coordinate))
                {
                    structure[coordinate] = tileType;
                }
            }
        }

        public static void CopyPointCloud(IReadOnlyDungeonStructure fromStructure, IEnumerable<Vector2Int> fromCoordinates, DungeonStructure toStructure, IEnumerable<Vector2Int> toCoordinates)
        {
            var pairs = fromCoordinates.Zip(toCoordinates, (a, b) => (a, b));
            foreach (var (from, to) in pairs)
                toStructure[to] = fromStructure[from];
        }
    }
}

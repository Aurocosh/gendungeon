﻿using GenDungeon.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public class DistributedRandomList<T>
    {
        private readonly DistributedRandom _distributedRandom;
        private readonly List<T> _values;

        public int Count => _values.Count;
        public T this[int index] => _values[index];

        public DistributedRandomList(Random random)
        {
            _distributedRandom = new DistributedRandom(random);
            _values = new List<T>();
        }

        public DistributedRandomList(Random random, IEnumerable<(T, double)> values) : this(random)
        {
            foreach (var (value, distribution) in values)
                Add(value, distribution);
        }

        public T GetRandom()
        {
            if (_values.Count == 0)
                return default;
            var index = _distributedRandom.GetRandomNumber();
            if (index < 0)
                return default;
            return _values[index];
        }

        public void Add(T value, double distribution)
        {
            var newIndex = _values.Count;
            _values.Add(value);
            _distributedRandom.AddNumber(newIndex, distribution);
        }

        public void Clear()
        {
            _values.Clear();
            _distributedRandom.Clear();
        }
    }
}

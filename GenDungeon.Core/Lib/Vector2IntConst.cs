﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public static class Vector2IntConst
    {
        public static Vector2Int PosY { get { return new Vector2Int(0, 1); } }
        public static Vector2Int PosX { get { return new Vector2Int(1, 0); } }
        public static Vector2Int NegY { get { return new Vector2Int(0, -1); } }
        public static Vector2Int NegX { get { return new Vector2Int(-1, 0); } }

        public static Vector2Int Up { get { return new Vector2Int(0, 1); } }
        public static Vector2Int Right { get { return new Vector2Int(1, 0); } }
        public static Vector2Int Down { get { return new Vector2Int(0, -1); } }
        public static Vector2Int Left { get { return new Vector2Int(-1, 0); } }

        public static Vector2Int PosXPosY { get { return new Vector2Int(1, 1); } }
        public static Vector2Int PosXNegY { get { return new Vector2Int(1, -1); } }
        public static Vector2Int NegXNegY { get { return new Vector2Int(-1, -1); } }
        public static Vector2Int NegXPosY { get { return new Vector2Int(-1, 1); } }

        private static readonly List<Vector2Int> _fourDirections = new List<Vector2Int> { PosY, PosX, NegY, NegX };
        public static IEnumerable<Vector2Int> FourDirections => _fourDirections;

        private static readonly List<Vector2Int> _eightDirections = new List<Vector2Int> { PosY, PosXPosY, PosX, PosXNegY, NegY, NegXNegY, NegX, NegXPosY };
        public static IEnumerable<Vector2Int> EightDirections => _eightDirections;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenDungeon.Core.Lib
{
    public static class IEnumerableExtensions
    {
        public static List<T> ToShuffledList<T>(this IEnumerable<T> input, Random random)
        {
            var list = input.ToList();
            list.Shuffle(random);
            return list;
        }
    }
}

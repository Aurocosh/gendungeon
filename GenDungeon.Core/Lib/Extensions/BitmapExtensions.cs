﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public static class BitmapExtensions
    {
        public static Color GetPixel(this Bitmap bitmap, Vector2Int coordinate)
        {
            return bitmap.GetPixel(coordinate.X, coordinate.Y);
        }
    }
}

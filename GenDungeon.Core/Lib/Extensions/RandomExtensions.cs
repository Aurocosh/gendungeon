﻿using GenDungeon.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public static class RandomExtensions
    {
        public static int Next(this Random random, IntRange intRange)
        {
            return random.Next(intRange.Min, intRange.Max);
        }

        public static int NextInclusive(this Random random, IntRange intRange)
        {
            return random.Next(intRange.Min, intRange.Max + 1);
        }
    }
}

﻿using GenDungeon.Core.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public static class RectangleExtensions
    {
        public static IntRange GetRangeX(this Rectangle rectangle)
        {
            return new IntRange(rectangle.X, rectangle.X + rectangle.Width);
        }

        public static IntRange GetRangeY(this Rectangle rectangle)
        {
            return new IntRange(rectangle.Y, rectangle.Y + rectangle.Height);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public static class DictionaryExtensions
    {
        public static V GetValueOrDefault<K, V>(this IDictionary<K, V> dictionary, K key, V defaultValue)
        {
            return dictionary.TryGetValue(key, out V value) ? value : defaultValue;
        }

        public static V GetValueOrLazyDefault<K, V>(this IDictionary<K, V> dictionary, K key, Func<V> defaultLambda)
        {
            return dictionary.TryGetValue(key, out V value) ? value : defaultLambda();
        }

        public static V ComputeIfAbsent<K, V>(this IDictionary<K, V> dictionary, K key, V defaultValue)
        {
            if (dictionary.TryGetValue(key, out V value))
            {
                return value;
            }
            else
            {
                dictionary[key] = defaultValue;
                return defaultValue;
            }
        }

        public static V ComputeIfAbsent<K, V>(this IDictionary<K, V> dictionary, K key, Func<V> defaultLambda)
        {
            if (dictionary.TryGetValue(key, out V value))
            {
                return value;
            }
            else
            {
                var defaultValue = defaultLambda();
                dictionary[key] = defaultValue;
                return defaultValue;
            }
        }

        public static V GetAndRemove<K, V>(this IDictionary<K, V> dictionary, K key)
        {
            if (dictionary.TryGetValue(key, out V value))
            {
                dictionary.Remove(key);
                return value;
            }

            throw new KeyNotFoundException();
        }
    }
}

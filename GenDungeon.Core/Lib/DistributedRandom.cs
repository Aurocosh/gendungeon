﻿using GenDungeon.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Lib
{
    public class DistributedRandom : IDeepCopy<DistributedRandom>
    {
        private double _distributionsSum;

        private readonly Random _random;
        private readonly Dictionary<int, double> _distributions;

        private readonly int _defaultNumber;

        public DistributedRandom(Random random, int defaultNumber = -1)
        {
            _random = random;
            _distributionsSum = 0;
            _defaultNumber = defaultNumber;
            _distributions = new Dictionary<int, double>();
        }

        public DistributedRandom(Random random, Dictionary<int, double> distributions, int defaultNumber)
        {
            _random = random;
            _defaultNumber = defaultNumber;
            _distributionsSum = distributions.Values.Sum();
            _distributions = new Dictionary<int, double>(distributions);
        }

        public int GetRandomNumber()
        {
            var rand = _random.NextDouble();
            var ratio = 1.0f / _distributionsSum;
            var tempDist = 0.0;
            foreach (var pair in _distributions)
            {
                tempDist += pair.Value;
                if (rand / ratio <= tempDist)
                    return pair.Key;
            }
            return _defaultNumber;
        }

        public void AddNumber(int index, double distribution)
        {
            if (!_distributions.TryGetValue(index, out var oldDistribution))
                oldDistribution = 0;
            _distributionsSum -= oldDistribution;
            _distributions[index] = distribution;
            _distributionsSum += distribution;
        }

        public void Clear()
        {
            _distributions.Clear();
            _distributionsSum = 0.0;
        }

        public DistributedRandom DeepCopy()
        {
            return new DistributedRandom(_random, _distributions, _defaultNumber);
        }
    }
}

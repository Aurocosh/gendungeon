﻿using GenDungeon.Core.Grids;
using GenDungeon.Core.Interfaces;
using GenDungeon.Core.Lib;
using GenDungeon.Core.StructureLogger;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Dungeon
{
    public interface IReadOnlyDungeonStructure : IReadOnlyGrid<int>, IDeepCopy<DungeonStructure>
    {
        StructureType StructureType { get; }
    }
}

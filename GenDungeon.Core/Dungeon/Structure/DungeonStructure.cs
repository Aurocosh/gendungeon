﻿using GenDungeon.Core.Grids;
using GenDungeon.Core.Interfaces;
using GenDungeon.Core.Lib;
using GenDungeon.Core.StructureLogger;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Dungeon
{
    public class DungeonStructure : Grid<int>, IReadOnlyDungeonStructure, IDeepCopy<DungeonStructure>
    {
        public StructureType StructureType { get; set; }

        public DungeonStructure(StructureType type = StructureType.DungeonStructure)
        {
            StructureType = type;
        }

        public DungeonStructure(int width, int height, StructureType type = StructureType.DungeonStructure) : base(width, height)
        {
            StructureType = type;
        }

        public DungeonStructure(int[,] tiles, StructureType type = StructureType.DungeonStructure) : base(tiles)
        {
            StructureType = type;
        }

        public DungeonStructure DeepCopy()
        {
            var tiles = new int[Width, Height];
            Array.Copy(_tiles, tiles, _tiles.Length);
            return new DungeonStructure(tiles, StructureType);
        }
    }
}

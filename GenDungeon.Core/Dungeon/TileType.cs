﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Dungeon
{
    public enum TileType
    {
        None,
        Floor,
        Wall,
        Door,
        Road,
        Water,
        Lava,
        Grass,
    }
}

﻿using GenDungeon.Core.Interfaces;
using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Dungeon
{
    public class RegionData : IDeepCopy<RegionData>
    {
        public Dictionary<int, List<Vector2Int>> RegionCoordinates;

        public RegionData()
        {
            RegionCoordinates = new Dictionary<int, List<Vector2Int>>();
        }

        public RegionData(Dictionary<int, List<Vector2Int>> regionCoordinates)
        {
            RegionCoordinates = regionCoordinates;
        }

        public RegionData DeepCopy()
        {
            var regionCoordinates = new Dictionary<int, List<Vector2Int>>(RegionCoordinates);
            return new RegionData(regionCoordinates);
        }
    }
}

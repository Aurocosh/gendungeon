﻿using GenDungeon.Core.InputData;
using GenDungeon.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Dungeon.Converter
{
    public static class DungeonStructureReflector
    {
        public static DungeonStructure Mirror(IReadOnlyDungeonStructure structure, StructureMirror mirror)
        {
            switch (mirror)
            {
                case StructureMirror.Normal:
                    return structure.DeepCopy();
                case StructureMirror.MirrorX:
                    return MirrorX(structure);
                case StructureMirror.MirrorY:
                    return MirrorY(structure);
            }
            throw new ArgumentException("Invalid mirror");
        }

        public static DungeonStructure MirrorX(IReadOnlyDungeonStructure structure)
        {
            int maxX = structure.Width - 1;
            var mirrored = new DungeonStructure(structure.Width, structure.Height);

            foreach (var coordinate in new SolidRectangle(structure.Rectangle))
            {
                int newX = maxX - coordinate.X;
                int newY = coordinate.Y;
                mirrored[newX, newY] = structure[coordinate];
            }

            return mirrored;
        }

        public static DungeonStructure MirrorY(IReadOnlyDungeonStructure structure)
        {
            int maxY = structure.Height - 1;
            var mirrored = new DungeonStructure(structure.Width, structure.Height);

            foreach (var coordinate in new SolidRectangle(structure.Rectangle))
            {
                int newX = coordinate.X;
                int newY = maxY - coordinate.Y;
                mirrored[newX, newY] = structure[coordinate];
            }

            return mirrored;
        }
    }
}

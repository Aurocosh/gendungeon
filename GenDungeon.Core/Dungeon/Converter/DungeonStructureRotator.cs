﻿using GenDungeon.Core.InputData;
using GenDungeon.Core.Interfaces;
using GenDungeon.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Dungeon.Converter
{
    public static class DungeonStructureRotator
    {
        public static DungeonStructure Rotate(IReadOnlyDungeonStructure structure, StructureRotation rotation)
        {
            switch (rotation)
            {
                case StructureRotation.Rotation0:
                    return structure.DeepCopy();
                case StructureRotation.Rotation90:
                    return Rotate90(structure);
                case StructureRotation.Rotation180:
                    return Rotate180(structure);
                case StructureRotation.Rotation270:
                    return Rotate270(structure);
            }
            throw new ArgumentException("Invalid rotation");
        }

        public static DungeonStructure Rotate90(IReadOnlyDungeonStructure structure)
        {
            int maxX = structure.Width - 1;
            var rotated = new DungeonStructure(structure.Height, structure.Width);

            foreach (var coordinate in new SolidRectangle(structure.Rectangle))
            {
                int newX = coordinate.Y;
                int newY = maxX - coordinate.X;
                rotated[newX, newY] = structure[coordinate];
            }

            return rotated;
        }

        public static DungeonStructure Rotate180(IReadOnlyDungeonStructure structure)
        {
            int maxX = structure.Width - 1;
            int maxY = structure.Height - 1;
            var rotated = new DungeonStructure(structure.Width, structure.Height);

            foreach (var coordinate in new SolidRectangle(structure.Rectangle))
            {
                int newX = maxX - coordinate.X;
                int newY = maxY - coordinate.Y;
                rotated[newX, newY] = structure[coordinate];
            }

            return rotated;
        }

        public static DungeonStructure Rotate270(IReadOnlyDungeonStructure structure)
        {
            int maxY = structure.Height - 1;
            var rotated = new DungeonStructure(structure.Height, structure.Width);

            foreach (var coordinate in new SolidRectangle(structure.Rectangle))
            {
                int newX = maxY - coordinate.Y;
                int newY = coordinate.X;
                rotated[newX, newY] = structure[coordinate];
            }

            return rotated;
        }
    }
}

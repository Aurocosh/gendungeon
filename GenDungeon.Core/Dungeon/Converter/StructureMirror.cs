﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Dungeon.Converter
{
    public enum StructureMirror
    {
        Normal,
        MirrorX,
        MirrorY
    }
}

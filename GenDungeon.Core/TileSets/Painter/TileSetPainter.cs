﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.TileSets.Painter
{
    public class TileSetPainter
    {
        private readonly TileSet _tileSet;

        public TileSetPainter(TileSet tileSet)
        {
            _tileSet = tileSet;
        }

        public DungeonStructure PaintDungeon(DungeonStructure schema)
        {
            int cellSize = _tileSet.CellSize;
            var width = schema.Width * cellSize;
            var height = schema.Height * cellSize;

            var structure = new DungeonStructure(width, height);
            foreach (var coordinate in new SolidRectangle(schema.Rectangle))
            {
                int tileId = schema[coordinate];
                var cellTemplate = _tileSet.GetTile(tileId).TileTemplate;
                var fromRect = new SolidRectangle(cellTemplate.Rectangle);

                var cellOrigin = coordinate * cellSize;
                var toRect = new SolidRectangle(cellOrigin, cellSize, cellSize);
                
                DungeonStructureDrawing.CopyPointCloud(cellTemplate, fromRect, structure, toRect);
            }

            return structure;
        }
    }
}

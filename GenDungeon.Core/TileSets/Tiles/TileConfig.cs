﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.InputData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Data.Tiles
{
    public class TileConfig
    {
        public IEnumerable<string> ConnectionsUp { get; }
        public IEnumerable<string> ConnectionsRight { get; }
        public IEnumerable<string> ConnectionsDown { get; }
        public IEnumerable<string> ConnectionsLeft { get; }
        public IEnumerable<StructureRotation> Rotations { get; }

        public TileConfig(IEnumerable<string> connectionsUp, IEnumerable<string> connectionsRight, IEnumerable<string> connectionsDown, IEnumerable<string> connectionsLeft, IEnumerable<StructureRotation> rotations)
        {
            ConnectionsUp = connectionsUp;
            ConnectionsRight = connectionsRight;
            ConnectionsDown = connectionsDown;
            ConnectionsLeft = connectionsLeft;
            Rotations = rotations;
        }
    }
}

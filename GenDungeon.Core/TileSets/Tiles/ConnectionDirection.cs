﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.TileSets.Tiles
{
    public enum ConnectionDirection
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3,
    }
}

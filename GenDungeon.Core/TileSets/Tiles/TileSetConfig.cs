﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Dungeon.Converter;
using GenDungeon.Core.InputData;
using GenDungeon.Core.Lib.Extensions;
using GenDungeon.Core.TileSets.Tiles;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Data.Tiles
{
    public class TileSetConfig
    {
        private readonly ImmutableList<string> _connectionNames;
        private readonly ImmutableDictionary<string, int> _connectionIdMap;

        public int CellSize { get; }
        public int BorderConnectionId { get; }

        public IEnumerable<string> ConnectionNames => _connectionNames;
        public IEnumerable<int> ConnectionIds => _connectionIdMap.Values;


        public TileSetConfig(int cellSize, string borderConnection, IEnumerable<string> connectionTypes)
        {
            CellSize = cellSize;

            _connectionNames = connectionTypes.Append(borderConnection).Distinct().ToImmutableList();
            _connectionIdMap = _connectionNames.Select((value, index) => (value, index)).ToImmutableDictionary(x => x.value, x => x.index);

            BorderConnectionId = _connectionIdMap[borderConnection];
        }

        public string GetConnectionName(int connectionId) => _connectionNames[connectionId];
        public int GetConnectionId(string connectionName) => _connectionIdMap[connectionName];

        public IEnumerable<Func<int, Tile>> GenerateTiles(string name, DungeonStructure structure, TileConfig tileConfig)
        {
            if (structure.Width != CellSize || structure.Height != CellSize)
                throw new ArgumentException($"Tile '{name}' cell size is not equal to declared cell size in tile set. Size of tile ({structure.Width}x{structure.Height}). Declared cell size {CellSize}.");

            return GenerateTileVariants(name, structure, tileConfig);
        }

        private IEnumerable<Func<int, Tile>> GenerateTileVariants(string name, DungeonStructure structure, TileConfig tileConfig)
        {
            foreach (var rotation in tileConfig.Rotations)
            {
                var tileStructure = DungeonStructureRotator.Rotate(structure, rotation);

                var connectionsUp = tileConfig.ConnectionsUp.Select(x => _connectionIdMap[x]);
                var connectionsRight = tileConfig.ConnectionsRight.Select(x => _connectionIdMap[x]);
                var connectionsDown = tileConfig.ConnectionsDown.Select(x => _connectionIdMap[x]);
                var connectionsLeft = tileConfig.ConnectionsLeft.Select(x => _connectionIdMap[x]);

                var connections = new List<IEnumerable<int>>() { connectionsUp, connectionsRight, connectionsDown, connectionsLeft };
                var rotationShift = ConnectionDirectionUtility.GetRotationCount(rotation);
                var rotatedConnections = connections.ShiftRight(rotationShift);

                var tileName = $"{name} - {rotation}";
                yield return id => new Tile(id, tileName, tileStructure, rotatedConnections);
            }
        }
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib.Extensions;
using GenDungeon.Core.TileSets.Tiles;
using GenDungeon.Core.Tools.Tiles;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Data.Tiles
{
    public class Tile
    {
        public int Id { get; }
        public string Name { get; }
        private readonly ImmutableList<ImmutableList<int>> _connections;

        public IReadOnlyDungeonStructure TileTemplate { get; }
        public IImmutableList<ConnectionState> ValidConnectionStates { get; }

        public Tile(int id, string name, DungeonStructure dungeonStructure, List<IEnumerable<int>> connections)
        {
            Id = id;
            Name = name;
            TileTemplate = dungeonStructure.DeepCopy();
            var connUp = connections[(int)ConnectionDirection.Up].Distinct().ToImmutableList();
            var connRight = connections[(int)ConnectionDirection.Right].Distinct().ToImmutableList();
            var connDown = connections[(int)ConnectionDirection.Down].Distinct().ToImmutableList();
            var connLeft = connections[(int)ConnectionDirection.Left].Distinct().ToImmutableList();

            _connections = ImmutableList.Create(connUp, connRight, connDown, connLeft);

            var validConnectionStates =
                from upId in connUp.Append(-1)
                from rightId in connRight.Append(-1)
                from downId in connDown.Append(-1)
                from leftId in connLeft.Append(-1)
                select new ConnectionState(upId, rightId, downId, leftId);

            ValidConnectionStates = validConnectionStates.ToImmutableList();
        }

        public IEnumerable<int> GetConnections(ConnectionDirection direction)
        {
            return _connections[(int)direction];
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

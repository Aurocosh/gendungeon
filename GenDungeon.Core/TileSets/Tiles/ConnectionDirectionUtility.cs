﻿using GenDungeon.Core.InputData;
using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.TileSets.Tiles
{
    public static class ConnectionDirectionUtility
    {
        private static readonly ImmutableList<ConnectionDirection> _connectionDirections = ImmutableList.Create(
            ConnectionDirection.Up, ConnectionDirection.Right, ConnectionDirection.Down, ConnectionDirection.Left);

        public static IEnumerable<ConnectionDirection> ConnectionDirections => _connectionDirections;

        public static ConnectionDirection GetOpposite(ConnectionDirection direction)
        {
            switch (direction)
            {
                case ConnectionDirection.Up:
                    return ConnectionDirection.Down;
                case ConnectionDirection.Right:
                    return ConnectionDirection.Left;
                case ConnectionDirection.Down:
                    return ConnectionDirection.Up;
                case ConnectionDirection.Left:
                    return ConnectionDirection.Right;
            }
            throw new ArgumentOutOfRangeException("Invalid direction value");
        }

        public static Vector2Int GetDirectionShift(ConnectionDirection direction)
        {
            switch (direction)
            {
                case ConnectionDirection.Up:
                    return Vector2IntConst.Up;
                case ConnectionDirection.Right:
                    return Vector2IntConst.Right;
                case ConnectionDirection.Down:
                    return Vector2IntConst.Down;
                case ConnectionDirection.Left:
                    return Vector2IntConst.Left;
            }
            throw new ArgumentOutOfRangeException("Invalid direction value");
        }

        public static int GetRotationCount(StructureRotation rotation)
        {
            switch (rotation)
            {
                case StructureRotation.Rotation0:
                    return 0;
                case StructureRotation.Rotation90:
                    return 1;
                case StructureRotation.Rotation180:
                    return 2;
                case StructureRotation.Rotation270:
                    return 3;
            }
            throw new ArgumentOutOfRangeException("Invalid rotation value");
        }
    }
}

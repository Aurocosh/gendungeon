﻿using GenDungeon.Core.Data.Tiles;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.TileSets
{
    public class TileSet
    {
        private readonly TileSetConfig _tileSetConfig;
        private readonly ImmutableList<Tile> _tiles;
        private readonly ImmutableList<int> _tileIds;

        public IEnumerable<Tile> Tiles => _tiles;
        public IEnumerable<int> TileIds => _tileIds;

        public IEnumerable<int> ConnectionIds => _tileSetConfig.ConnectionIds;
        public IEnumerable<string> ConnectionNames => _tileSetConfig.ConnectionNames;

        public int CellSize => _tileSetConfig.CellSize;
        public int BorderConnectionId => _tileSetConfig.BorderConnectionId;

        public TileSet(TileSetConfig tileSetConfig, IEnumerable<Func<int, Tile>> tiles)
        {
            int nextTileId = 0;
            _tileSetConfig = tileSetConfig;
            _tiles = tiles.Select(x => x.Invoke(nextTileId++)).ToImmutableList();
            _tileIds = _tiles.Select(x => x.Id).ToImmutableList();
        }

        public Tile GetTile(int id) => _tiles[id];
        public string GetConnectionName(int connectionId) => _tileSetConfig.GetConnectionName(connectionId);
        public int GetConnectionId(string connectionName) => _tileSetConfig.GetConnectionId(connectionName);
    }
}

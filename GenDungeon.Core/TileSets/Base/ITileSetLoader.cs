﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.TileSets
{
    public interface ITileSetLoader
    {
        TileSet LoadTileSet(string name);
    }
}

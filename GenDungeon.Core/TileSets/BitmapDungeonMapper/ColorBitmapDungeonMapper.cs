﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using GenDungeon.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.InputData.BitmapDungeonMapper
{
    public class ColorBitmapDungeonMapper : IBitmapDungeonMapper
    {
        private readonly Dictionary<Color, int> _colorMapping;

        public ColorBitmapDungeonMapper(Dictionary<Color, int> colorMapping)
        {
            _colorMapping = new Dictionary<Color, int>(colorMapping);
        }

        public ColorBitmapDungeonMapper(Dictionary<string, int> colorMapping)
        {
            _colorMapping = colorMapping.ToDictionary(x => ColorTranslator.FromHtml(x.Key), x => x.Value);
        }

        public DungeonStructure Convert(Bitmap bitmap)
        {
            var maxY = bitmap.Height - 1;
            var structure = new DungeonStructure(bitmap.Width, bitmap.Height);
            foreach (var coordinate in new SolidRectangle(structure.Rectangle))
            {
                var color = bitmap.GetPixel(coordinate.X, maxY - coordinate.Y);
                structure[coordinate] = _colorMapping[color];
            }

            return structure;
        }
    }
}

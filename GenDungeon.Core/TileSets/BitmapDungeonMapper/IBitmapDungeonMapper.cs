﻿using GenDungeon.Core.Dungeon;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.InputData.BitmapDungeonMapper
{
    public interface IBitmapDungeonMapper
    {
        DungeonStructure Convert(Bitmap bitmap);
    }
}

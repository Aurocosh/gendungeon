﻿using GenDungeon.Core.Data.Tiles;
using GenDungeon.Core.InputData.BitmapDungeonMapper;
using GenDungeon.Core.InputData.Tiles;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;

namespace GenDungeon.Core.TileSets
{
    public class TileSetLoader : ITileSetLoader
    {
        private readonly string _sourceFolder;
        private readonly IDeserializer _deserializer;

        public TileSetLoader(string sourceFolder)
        {
            _sourceFolder = sourceFolder;

            _deserializer = new DeserializerBuilder()
                .WithTagMapping("!TileConfigData", typeof(TileConfigData))
                .WithTagMapping("!TileSetConfigData", typeof(TileSetConfigData))
                .WithTagMapping("!ColorMapping", typeof(ColorMapping))
                .Build();
        }

        public TileSet LoadTileSet(string name)
        {
            var tileSetFolder = Path.Combine(_sourceFolder, "TileSets", name);

            var configFile = Path.Combine(tileSetFolder, "Config.yaml");
            var tileSetConfig = LoadYaml<TileSetConfigData>(configFile).GetData();

            var colorSetFile = Path.Combine(tileSetFolder, "ColorMapper.yaml");
            var colorMapping = LoadYaml<ColorMapping>(colorSetFile);
            var bitmapMapper = new ColorBitmapDungeonMapper(colorMapping.ToIntMapping());

            var tilesFolder = Path.Combine(tileSetFolder, "Tiles");
            var tileFiles = Directory.GetFiles(tilesFolder, "*.yaml", SearchOption.AllDirectories);
            var tiles = new List<Func<int, Tile>>();
            foreach (var tileSetFile in tileFiles)
            {
                var tileName = Path.GetFileNameWithoutExtension(tileSetFile);
                var tileDirectory = Path.GetDirectoryName(tileSetFile);
                var bitmapPath = Path.Combine(tileDirectory, $"{tileName}.bmp");

                var bitmap = new Bitmap(bitmapPath);
                var tileConfig = LoadYaml<TileConfigData>(tileSetFile).GetData();

                var structure = bitmapMapper.Convert(bitmap);
                var tileVariants = tileSetConfig.GenerateTiles(tileName, structure, tileConfig);
                tiles.AddRange(tileVariants);
            }

            return new TileSet(tileSetConfig, tiles);
        }

        private T LoadYaml<T>(string filePath)
        {
            using (var streamReader = new StreamReader(filePath))
                return _deserializer.Deserialize<T>(streamReader);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Interfaces
{
    public interface IDeepCopy<T>
    {
        T DeepCopy();
    }
}

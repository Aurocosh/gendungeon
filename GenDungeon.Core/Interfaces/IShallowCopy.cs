﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Interfaces
{
    public interface IShallowCopy<T>
    {
        T ShallowCopy();
    }
}

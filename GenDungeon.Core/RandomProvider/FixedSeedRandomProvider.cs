﻿using GenDungeon.Core.RandomProvider.Base;
using System;
using System.Collections.Generic;

namespace GenDungeon.Core.RandomProvider
{
    public class FixedSeedRandomProvider : IRandomProvider
    {
        public int Seed { get; set; }

        public FixedSeedRandomProvider()
        {
        }

        public FixedSeedRandomProvider(int seed)
        {
            Seed = seed;
        }

        public Random GetRandom()
        {
            return new Random(Seed);
        }
    }
}

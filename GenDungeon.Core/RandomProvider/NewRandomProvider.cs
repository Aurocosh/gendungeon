﻿using GenDungeon.Core.RandomProvider.Base;
using System;
using System.Collections.Generic;

namespace GenDungeon.Core.RandomProvider
{
    public class NewRandomProvider : IRandomProvider
    {
        public NewRandomProvider()
        {
        }

        public Random GetRandom()
        {
            return new Random();
        }
    }
}

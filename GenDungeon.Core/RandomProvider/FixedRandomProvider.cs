﻿using GenDungeon.Core.RandomProvider.Base;
using System;
using System.Collections.Generic;

namespace GenDungeon.Core.RandomProvider
{
    public class FixedRandomProvider : IRandomProvider
    {
        private readonly Random _random;

        public FixedRandomProvider()
        {
            _random = new Random();
        }

        public Random GetRandom()
        {
            return _random;
        }
    }
}

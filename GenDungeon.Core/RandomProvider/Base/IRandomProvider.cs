﻿using System;
using System.Collections.Generic;

namespace GenDungeon.Core.RandomProvider.Base
{
    public interface IRandomProvider
    {
        Random GetRandom();
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using GenDungeon.Core.ValueExtractors.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ValueExtractors
{
    public class GenericValueExtractor : IValueExtractor
    {
        private readonly List<Vector2Int> _coordinateShifts;

        public GenericValueExtractor(IEnumerable<Vector2Int> coordinateShifts)
        {
            _coordinateShifts = coordinateShifts.ToList();
        }

        public IEnumerable<int> ExtractValues(IReadOnlyDungeonStructure source, Vector2Int origin)
        {
            foreach (var shift in _coordinateShifts)
            {
                var coordinate = origin + shift;
                if (source.IsItInBounds(coordinate))
                    yield return source[coordinate];
            }
        }
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.ValueExtractors.Base
{
    public interface IValueExtractor
    {
        IEnumerable<int> ExtractValues(IReadOnlyDungeonStructure source, Vector2Int origin);
    }
}

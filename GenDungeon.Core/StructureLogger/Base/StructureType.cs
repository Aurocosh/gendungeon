﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.StructureLogger
{
    public enum StructureType
    {
        DungeonStructure,
        RegionMap
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.StructureLogger
{
    public enum StructureLogLevel
    {
        Low = 1,
        Medium = 2,
        High = 3
    }
}

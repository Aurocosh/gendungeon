﻿using GenDungeon.Core.Dungeon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.StructureLogger
{
    public interface IStructureLogger
    {
        StructureLogLevel LogLevel { get; set; }
        void LogStructure(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure);
        void LogPartialStructure(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure);
        void AddPartialState(string name, StructureLogLevel logLevel, StructureType structureType, DungeonStructure structure);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Utility
{
    public static class CoordinateUtilis
    {
        public static int WrapCoordinate(int value, int maxValue)
        {
            if (value < 0)
                return (value + maxValue) % maxValue;
            else if (value >= maxValue)
                return value % maxValue;
            else
                return value;
        }
    }
}

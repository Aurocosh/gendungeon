﻿using GenDungeon.Core.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Utility
{
    public static class IEnumerableUtils
    {
        public static Dictionary<T, int> CountDistinctItems<T>(IEnumerable<T> list)
        {
            var occuranceCounter = new Dictionary<T, int>();

            foreach (var item in list)
            {
                var occurances = occuranceCounter.GetValueOrDefault(item, 0);
                occuranceCounter[item] = occurances++;
            }

            return occuranceCounter;
        }
    }
}

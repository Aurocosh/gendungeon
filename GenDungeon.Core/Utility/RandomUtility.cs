﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Core.Utility
{
    public static class RandomUtility
    {
        private static readonly Random _random = new Random();

        public static bool RollDice(int chance) => _random.Next(0, 101) < chance;
        public static bool RollDice(float chance) => _random.NextDouble() < chance;
        public static bool RollDice(double chance) => _random.NextDouble() < chance;

        public static bool RollDice(int chance, Random random) => random.Next(0, 101) < chance;
        public static bool RollDice(float chance, Random random) => random.NextDouble() < chance;
        public static bool RollDice(double chance, Random random) => random.NextDouble() < chance;
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenDungeon.Test.Utility
{
    public static class StructureComparator
    {
        public static bool AreStructuresEqual(DungeonStructure first, DungeonStructure second)
        {
            if (first.Width != second.Width)
                return false;
            if (first.Height != second.Height)
                return false;

            foreach (var coordinate in new SolidRectangle(first.Rectangle))
            {
                if (first[coordinate] != second[coordinate])
                {
                    return false;
                }
            }
            return true;
        }
    }
}

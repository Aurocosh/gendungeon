﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Dungeon.Converter;
using GenDungeon.Core.Primitives;
using GenDungeon.Test.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GenDungeon.Test.RotationTests
{
    public class RotationBy90Tests
    {
        [Fact]
        public void Rotate2By2By90Test()
        {
            var input = new int[,] {
                { 1, 2},
                { 3, 4}
            };

            var result = new int[,] {
                { 3, 1 },
                { 4, 2 }
            };

            RotateBy90AndCompare(input, result);
        }

        [Fact]
        public void Rotate3By3By90Test()
        {
            var input = new int[,] {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };

            var result = new int[,] {
                { 7, 4, 1 },
                { 8, 5, 2 },
                { 9, 6, 3 }
            };

            RotateBy90AndCompare(input, result);
        }

        private static void RotateBy90AndCompare(int[,] input, int[,] result)
        {
            var inputStructure = new DungeonStructure(input);
            var resutStructure = new DungeonStructure(result);

            var rotatedStructure = DungeonStructureRotator.Rotate90(inputStructure);

            Assert.True(StructureComparator.AreStructuresEqual(rotatedStructure, resutStructure));
        }
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Dungeon.Converter;
using GenDungeon.Core.Primitives;
using GenDungeon.Test.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GenDungeon.Test.RotationTests
{
    public class RotationBy180Tests
    {
        [Fact]
        public void Rotate2By2By180Test()
        {
            var input = new int[,] {
                { 1, 2},
                { 3, 4}
            };

            var result = new int[,] {
                { 4, 3 },
                { 2, 1 }
            };

            RotateBy180AndCompare(input, result);
        }

        [Fact]
        public void Rotate3By3By180Test()
        {
            var input = new int[,] {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };

            var result = new int[,] {
                { 9, 8, 7 },
                { 6, 5, 4 },
                { 3, 2, 1 }
            };

            RotateBy180AndCompare(input, result);
        }

        private static void RotateBy180AndCompare(int[,] input, int[,] result)
        {
            var inputStructure = new DungeonStructure(input);
            var resutStructure = new DungeonStructure(result);

            var rotatedStructure = DungeonStructureRotator.Rotate180(inputStructure);

            Assert.True(StructureComparator.AreStructuresEqual(rotatedStructure, resutStructure));
        }
    }
}

﻿using GenDungeon.Core.Dungeon;
using GenDungeon.Core.Dungeon.Converter;
using GenDungeon.Core.Primitives;
using GenDungeon.Test.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GenDungeon.Test.RotationTests
{
    public class RotationBy270Tests
    {
        [Fact]
        public void Rotate2By2By270Test()
        {
            var input = new int[,] {
                { 1, 2},
                { 3, 4}
            };

            var result = new int[,] {
                { 2, 4 },
                { 1, 3 }
            };

            RotateBy270AndCompare(input, result);
        }

        [Fact]
        public void Rotate3By3By270Test()
        {
            var input = new int[,] {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };

            var result = new int[,] {
                { 3, 6, 9 },
                { 2, 5, 8 },
                { 1, 4, 7 }
            };

            RotateBy270AndCompare(input, result);
        }

        private static void RotateBy270AndCompare(int[,] input, int[,] result)
        {
            var inputStructure = new DungeonStructure(input);
            var resutStructure = new DungeonStructure(result);

            var rotatedStructure = DungeonStructureRotator.Rotate270(inputStructure);

            Assert.True(StructureComparator.AreStructuresEqual(rotatedStructure, resutStructure));
        }
    }
}
